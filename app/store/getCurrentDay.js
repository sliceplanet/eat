const days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
let date = new Date();
let currentDay = days[date.getDay()];
export default currentDay;
