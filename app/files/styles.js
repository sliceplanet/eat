'use strict';

var React = require('react-native');

var {
    StyleSheet,
    Dimensions
} = React;

const width = Dimensions.get('window').width; //full width
const height = Dimensions.get('window').height; //full height

module.exports = StyleSheet.create({
  header: {
    height: height * 0.75,
    display: 'flex',
    flexDirection: 'column',
  },
  hidden: {
    display: 'none',
  },
  headerImg: {
    height: height * 0.3,
    resizeMode: 'cover',
  },
  headerStarAndReview: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  headerRestName: {
    fontSize: 32,
    color: '#333',
    fontFamily: 'Ubuntu-Light',
    marginTop: 40
  },
  headerRestDescription: {
    fontFamily: 'HindVadodara-Light',
    fontSize: 16,
    color: '#535353'
  },
  headerReview: {
    fontFamily: 'HindVadodara-Medium',
    fontSize: 14,
    color: '#266abd'
  },
  headerWhiteTransparentLine: {
    position: 'absolute',
    backgroundColor: '#fff',
    width: '110%',
    left: -2,
    bottom: -20,
    height: 35,
    transform: [{ rotate: '5deg'}]
  },
  headerWhiteTransparentLineAfter: {
    position: 'absolute',
    backgroundColor: '#fff',
    opacity: 0.48,
    width: '110%',
    left: -15,
    bottom: -15,
    height: 55,
    transform: [{ rotate: '13.5deg'}]
  },
  headerWhiteTransparentLineBefore: {
    position: 'absolute',
    backgroundColor: '#fff',
    opacity: 0.48,
    width: '110%',
    left: -10,
    bottom: -7,
    height: 35,
    transform: [{ rotate: '9.5deg'}]
  },
  headerLogoBack: {
    position: 'absolute',
    top: height * 0.3 - 75,
    height: 100,
    width: 100,
    backgroundColor: '#fff',
    borderRadius: 5,
    zIndex: 1,
    alignSelf: 'center'
  },
  headerLogo: {
    height: 96,
    width: 96
  },
  headerBar: {
    position: 'absolute',
    top: 0,
    left: 0,
    display: 'flex',
    justifyContent: 'space-around',
    width: width,
    padding: 5,
    marginTop: 5,
  },
  headerBottom: {
    backgroundColor: 'white',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: width,
    padding: 5,
    paddingBottom: 0,
    justifyContent: 'center',
    height: height * 0.35
  },
  nav: {
    backgroundColor: '#fff',
    width: width,
    height: height * 0.1,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#eee'
  },
  navBtn: {
    height: '100%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 2,
    borderStyle: 'solid',
    borderColor: '#eee'
  },
  navBtnActive: {
    borderBottomWidth: 3,
    borderColor: '#266abd'
  },
  navBtnText: {
    fontFamily: 'Hind-Regular',
    fontSize: 12,
    color: '#333'
  },
  phoneicon: {
    height: 20,
    width: 20,
  },
  language: {
    color: '#fff',
    fontSize: 20,
  },
  logoWrapp: {
    height: 20,
    width: width * 0.6,
  },
  logo: {
    height: '100%',
    width: '100%',
    resizeMode: 'contain',
  },
  mainContent: {
    flex: 1,
    zIndex: -1,
  },
  footer: {
    backgroundColor: '#fff',
    width: '100%',
    flexDirection: 'column',
    display: 'flex',
  },
  footerTop: {
    padding: 30,
  },
  footerBottom: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 25,
  },
  footerBottomText: {
    fontSize: 14,
    fontFamily: 'Hind-Medium',
    fontWeight: '500',
    color: '#333',
    textAlign: 'center',
    marginTop: 10,
  },
  footerBottomTextMarginTop: {
    marginTop: width * 0.04,
    fontWeight: '300',
  },
  footerRainbow: {
    width: width,
    display: 'flex',
    flexDirection: 'row',
    flexGrow: 1,
    height: 4,
  },
  footerRainbowred: {
    flex: 1,
    backgroundColor: '#fa0029',
  },
  footerRainbowgreen: {
    flex: 1,
    backgroundColor: '#00ac41',
  },
  footerRainbowsalad: {
    flex: 1,
    backgroundColor: '#95d600',
  },
  footerRainbowblue: {
    flex: 1,
    backgroundColor: '#2f7de1',
  },
  footerRainbowlightblue: {
    flex: 1,
    backgroundColor: '#7dcaeb',
  },
  footerRainbowyellow: {
    flex: 1,
    backgroundColor: '#ffd700',
  },
  downloadApps: {
    fontSize: 16,
    fontFamily: 'Ubuntu-Medium',
    fontWeight: '500',
    color: '#333',
    textAlign: 'center',
  },
  forIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 16,
  },
  apps__icon: {
    height: 38.2,
    width: 130,
    marginLeft: 10,
    marginRight: 10,
  },
  logoFooter: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo__icon: {
    height: 60,
    width: 165,
  },
  social: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 26,
  },
  social_icon: {
    height: 28,
    width: 28,
    marginLeft: 10,
    marginRight: 10,
  },
  apps: {
    justifyContent: 'center',
    flexDirection: 'column',
    display: 'flex',
    marginTop: 26,
  },
  productBlock: {
    padding: 16,
    borderWidth: 1,
    width: '100%',
    borderColor: 'red',
    backgroundColor: "#fff",
  },
  productBlockNamePrice: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    marginBottom: 6,
  },
  productBlockPriceIcon: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  plusIcon: {
    height: 25,
    width: 25,
    marginLeft: 6,
  },
  productBlockPrice: {
    fontSize: 16,
    color: '#535353',
  },
  productBlockDescriptionText: {
    fontSize: 16,
    color: '#535353',
  },
  productBlockName: {
    fontSize: 20,
    color: '#535353',
  },
  orderNow: {
    height: 48,
    width: '90%',
    backgroundColor: '#006ac1',
    borderWidth: 0,
    borderLeftWidth: 16,
    borderRightWidth: 16,
    borderColor: '#005092',
    borderRadius: 5,
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 24,
    fontFamily: 'Ubuntu',
  },
  btnTextStyle: {
    fontSize: 16,
    color: '#fff',
  },
  popupStyle: {
    padding: width * 0.04,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 16,
  },
  popupRestStyle: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 16,
  },
  containerPopup: {

  },
  dialogStyle: {
    width: "90%",
    height: '35%',
    display: 'flex',
    position: 'relative',
  },
  dialogRestStyle: {
    width: "90%",
    height: '75%',
    display: 'flex',
    position: 'relative',
    backgroundColor: '#eaeaea',
    overflow: 'hidden',
  },
  postCodeInputWrap: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 24,
  },
  postCodeInput: {
    height: 54,
    width: '80%',
    borderRadius: 4,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
    borderColor: '#e6e6e6',
    borderWidth: 1,
    fontSize: 16,
    color: '#333',
    paddingLeft: 22,
  },
  goBtn: {
    height: 54,
    width: 48,
    backgroundColor: '#006ac1',
    borderRadius: 5,
    justifyContent: 'space-around',
    alignItems: 'center',
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
  },
  popupEP: {
    fontSize: 24,
    color: '#333',
    textAlign: 'center',
  },
  popupSI: {
    fontSize: 14,
    color: '#535353',
    marginTop: 12,
    fontFamily: 'Hind Vadodara',
  },
  wrapPopupCloseIcon: {
    position: 'absolute',
    top: 0,
    right: width * 0.04,
  },
  popupCloseIcon: {
    height: 25,
    width: 25,
  },
  popupRestHeader: {
    fontSize: 24,
    textAlign: 'center',
    color: '#333',
    marginTop: 24,
    width: '80%',
    marginBottom: 24,
  },
  popupRestWrapp: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: '#fff',
  },
  restDownBlockPopup: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    borderBottomWidth: 1,
    borderColor: '#eaeaea',
    padding: width * 0.035,
  },
  rdbName: {
    fontSize: 20,
    color: '#535353',
    marginBottom: width * 0.02,
    maxWidth: '65%',
    fontFamily: 'Hind Vadodara',
  },
  rdbDesc: {
    fontSize: 14,
    color: '#535353',
    marginBottom: width * 0.02,
    fontFamily: 'Hind Vadodara',
    fontWeight: '300',
  },
  rdbBlueText: {
    color: '#266abd',
    fontSize: 14,
    marginLeft: width * 0.01,
    fontFamily: 'Hind Vadodara',
    fontWeight: '500',
  },
  rdbSRwrapp: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  popupNews: {
  	fontSize: 14,
  	textAlign: 'center',
  	color: '#266abd',
  	marginTop: 16,
    width: '80%',
  },
  postcodeError: {
    fontSize: 14,
  	textAlign: 'center',
  	color: 'red',
  	marginTop: 16,
    width: '80%',
  },
  restUpBlockPopup: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    position: 'relative',
  },
  forMiniImg: {
    position: 'absolute',
    bottom: '-20%',
    right: '10%',
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#eaeaea',
    borderRadius: 5,
    overflow: 'hidden',
  },
  miniLogo: {
    height: 55,
    width: 55,
    resizeMode: 'contain',
  },
  featured: {
    fontSize: 12,
    color: '#535353',
  },
  forFeatured: {
    position: 'absolute',
    left: 5,
    top: 5,
    backgroundColor: '#f5f5f5',
    padding: width * 0.01,
    borderRadius: 2,
  },
  popupAllergyHeader: {
    fontSize: 24,
    color: '#333',
    textAlign: 'center',
    width: '100%',
    fontFamily: 'Ubuntu',
  },
  dialogAllergyStyle: {
    height: '35%',
    width: '90%',
  },
  commentWrapper: {
    backgroundColor: '#fff',
    width: width,
    display: 'flex',
    alignItems: 'center',
    paddingTop: 10,
  },
  wrap2: {
    display: 'flex',
  },
  wrap1: {

  },
  horLine: {
    backgroundColor: 'rgba(51,51,51,.24)',
    width: '100%',
    height: 1,
  },
  authorData: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection:'row',
  },
  comment__username: {
    color: '#333',
    fontSize: 16,
    fontFamily: 'Hind-Regular',
  },
  comment__date: {
    color: '#333',
    fontSize: 16,
    fontFamily: 'Hind-Light',
    fontWeight: '300',
  },
  comment__text: {
    fontFamily: 'Hind-Light',
    fontSize: 16,
    fontWeight: '300',
    color: '#333333',
  },
  comment: {
    paddingTop: 16,
    paddingBottom: 16,
    fontFamily: 'Hind',
    color: '#333',
    fontSize: 16,
  },
  wrappForSwitcher: {
    padding: 8,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
  },
  wrappForPicker: {
    padding: 8,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
  },
  dropdown_3: {
    width: 150,
    borderRadius: 1,
    padding: 8,
  },
  dropdown_3_dropdownTextStyle: {
    backgroundColor: '#fff',
    fontSize: 16,
    width: 150,
    color: '#266abd',
  },
  dropdown_3_dropdownTextHighlightStyle: {
    backgroundColor: '#266abd',
    color: '#fff',
  },
  textDropdown: {
    fontFamily: 'Ubuntu-Light',
    fontSize: 20,
    textAlign: 'center',
    color: '#266abd',
  },
  whiteTransparentLineAfter: {
    position: 'absolute',
    backgroundColor: '#fff',
    opacity: 0.48,
    width: '110%',
    left: -2,
    bottom: -16,
    height: 35,
     transform: [{ rotate: '5deg'}]
  },
  whiteTransparentLineBefore: {
    position: 'absolute',
    backgroundColor: '#fff',
    opacity: 0.48,
    width: '110%',
    left: -2,
    bottom: -8,
    height: 20,
    transform: [{ rotate: '3deg'}]
  },
  containerSlider: {
    flex: 1
  },
  slide: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'transparent',
    width: width,
    alignItems: 'center',
    paddingTop: width * 0.025,
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold'
  },
  imageSlide: {
    width: '95%',
    flex: 1
  },
  textSlide: {
    textAlign: 'center',
    margin: 0,
    fontFamily: 'HindVadodara-Light',
    fontSize: 16,
    fontWeight: '300',
    color: '#535353',
    marginTop: width * 0.025,
    marginBottom: width * 0.025,
  },
  sliderWrapper: {
    backgroundColor: "#fff",
  },
  comWrapp: {
    width: width,
    padding: 16,
  },
  overallWrapp: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 16,
  },
  overall: {
    backgroundColor: '#fff',
    width: width,
    borderRadius: 8,
    padding: 16,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    marginTop: 26,
  },
  overallHeading: {
    fontFamily: 'Ubuntu',
    fontSize: 20,
    color: '#333',
    marginTop: 8,
    marginBottom: 8,
  },
  overallThreeHeading: {
    fontFamily: 'Hind-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#333333',
  },
  rating: {
    fontFamily: 'Ubuntu',
    fontSize: 20,
    fontWeight: '500',
    textAlign: 'center',
    color: '#333333',
  },
  maximum: {
    opacity: 0.48,
    fontFamily: 'Hind',
    fontSize: 14,
    color: '#333333',
  },
  forGlobalRating: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  forGlobalRatingNumbers: {
    display: 'flex',
    flexDirection: 'row',
    marginLeft: 26,
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  forThreeGlobalRatingNumbers: {
    display: 'flex',
    flexDirection: 'row',
    marginLeft: 10,
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  basedOn: {
    fontFamily: 'Hind-Medium',
    textAlign: 'center',
    color: '#333',
    marginTop: 16,
    fontSize: 16,
    fontWeight: '500',
  },
  basedOnSmall: {
    fontFamily: 'Hind-Regular',
    fontSize: 14,
  },
  QDDwrapp: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    marginTop: 26,
  },
  marginBottom16: {
    marginBottom: 26,
  },
  qddrating: {
    fontFamily: 'Hind',
    color: '#333',
  },
  qddratingLeft: {
    fontWeight: '500',
  },
  forRatingBar: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: 16,
  },
  amountPercentForLineProgress: {
    fontFamily: 'Hind',
    fontSize: 14,
    fontWeight: '300',
    textAlign: 'right',
    color: '#333',
    position: 'absolute',
    right: 2,
  },
  forLineProgressWrapp: {
    position: 'relative',
  },
  mapBlock: {
    backgroundColor: '#fff',
    width: width,
    borderRadius: 8,
    padding: 20,
    display: 'flex',
    flexDirection: 'column',
  },
  mapHeading: {
    fontSize: 24,
    fontFamily: 'Ubuntu-Light',
    color: '#333',
    padding: 0,
    marginBottom: 16,
    fontWeight: '300',
  },
  mapMap: {
    borderColor: 'red',
    borderWidth: 1,
    width: '100%',
    height: 100,
    marginBottom: 16,
  },
  mapFooter: {
    display: 'flex',
    flexDirection: 'row'
  },
  map__info: {
    marginLeft: 10,
    display: 'flex',
    justifyContent: 'center',
  },
  mapIcon: {
    height: 64,
    width: 64,
  },
  apcInfo: {
    margin: 0,
    color: '#333',
    fontSize: 16,
    fontFamily: 'Hind-Regular'
  },
  ccInfo: {
    marginTop: 4,
    fontFamily: 'HindVadodara-Light',
    fontSize: 14,
    fontWeight: '300',
    color: '#535353',
  },
  delcolInfo: {
    marginBottom: 16,
  },
  deliverColection__type: {
    width: 160,
    borderRadius: 8,
    backgroundColor: '#f2fae2',
    marginRight: 16,
    textAlign: 'center',
    paddingTop: 8,
    paddingBottom: 8,
  },
  deliverColection__name: {
    margin: 0,
    fontFamily: 'Hind Vadodara',
    fontSize: 14,
    fontWeight: '300',
    textAlign: 'center',
    color: '#04822c',
  },
  deliverColection__ico: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  openingTimes__list: {

  },
  openingTimes__item: {
    fontFamily: 'Hind Vadodara',
    fontSize: 14,
    fontWeight: '300',
    color: '#535353',
    height: 32,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#eaeaea',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  openingTimes__itemActive: {
    fontSize: 14,
    fontWeight: '500',
    color: 'red',
  },
  niHauDesc: {
    fontFamily: 'HindVadodara-Light',
    fontSize: 14,
    fontWeight: '300',
    color: '#535353'
  },
  niHauHeading: {
    fontFamily: 'Ubuntu-Light',
    fontSize: 24,
    fontWeight: '300',
    color: '#333',
    marginBottom: 16,
  },
  auHeading: {
    fontFamily: 'Ubuntu-Light',
    fontSize: 20,
    color: '#333',
    marginBottom: 26,
    textAlign: 'center',
  },
  auProductBlock: {

  },
  auProductBlockTop: {
    height: width * 0.35,
    width: width,
    display: 'flex',
  },
  auProductBlockPhoto: {
    resizeMode: 'cover',
    height: '100%',
    width: '100%',
  },
  auProductBottomBlock: {
    display: 'flex',
    flexDirection: 'column',
    padding: 16,
    backgroundColor: '#fff',
  },
  auProductDishName: {
    fontFamily: 'HindVadodara-Light',
    fontSize: 20,
    fontWeight: '300',
    color: '#333333',
  },
  auProductDishDesc: {
    fontFamily: 'HindVadodara-Light',
    fontSize: 14,
    fontWeight: '300',
    color: '#535353',
  },
  auProductGreyLine: {
    height: 1,
    backgroundColor: '#f5f5f5',
    marginTop: 16,
    marginBottom: 16
  },
  auProductViewMenu: {
    fontFamily: 'Ubuntu Medium',
    fontSize: 16,
    fontWeight: '500',
    color: "#266abd",
    textAlign: 'center',
    marginBottom: 8,
  },
  storeBlock: {
    backgroundColor: '#fff',
    padding: 16,
    marginBottom: 8,
  },
  storeName: {
    fontFamily: 'Ubuntu-Light',
    fontSize: 20,
    fontWeight: '300',
    color: '#266abd',
    marginBottom: 8,
  },
  storeAddress: {
    fontFamily: 'Ubuntu-Light',
    fontSize: 20,
    fontWeight: '300',
    color: '#757575',
  },
  offerBlock: {
    backgroundColor: '#fff',
    padding: 16,
    marginBottom: 8,
  },
  offerTitle: {
    fontFamily: 'Hind Vadodara',
    fontSize: 20,
    fontWeight: '500',
    color: '#535353',
    marginBottom: 8,
  },
  offerText: {
    fontFamily: 'Hind Vadodara',
    fontSize: 14,
    fontWeight: '300',
    color: '#535353',
    marginBottom: 8,
    marginTop: 8,
  },
  offerLinkWrapper: {
    marginTop: 8,
    marginBottom: 8,
  },
});
