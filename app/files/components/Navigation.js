import React, {Component} from 'react'
// rn comoponents
import {
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Image
} from 'react-native'

// Other components
import {
  CardSection,
  Button,
} from './'

// Styles
import styles from '../../files/styles'

// Get full screen width and height
const {width, height} = Dimensions.get('window')

export class Navigation extends Component {
  constructor(props){
    super(props)
    this.state = {
      menu: null,
      info: null,
      aboutUs: null,
      reviews: null
    }
  }

  componentDidMount(){
    this.setActiveTab()
  }

  setActiveTab() {
    switch (this.props.navigation.state.routeName) {
      case 'Menu':
        this.setState({menu: styles.navBtnActive})
        break;
      case 'Info':
        this.setState({info: styles.navBtnActive})
        break;
      case 'AboutUs':
        this.setState({aboutUs: styles.navBtnActive})
        break;
      case 'Reviews':
        this.setState({reviews: styles.navBtnActive})
        break;
      default:
    }
  }

  goToMenu = () => {
    this.props.navigation.navigate('Menu')
  }

  goToInfo = () => {
    this.props.navigation.navigate('Info')
  }

  goToReviews = () => {
    this.props.navigation.navigate('Reviews')
  }

  goToAboutUs = () => {
    this.props.navigation.navigate('AboutUs')
  }

  render() {
    return (
      <CardSection style={[styles.nav, this.props.navStyle]}>
        <TouchableOpacity style={[styles.navBtn, this.state.menu]} onPress={this.goToMenu}>
          <Button btnStyle={styles.navBtnText}>
            Menu
          </Button>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.navBtn, this.state.reviews]} onPress={this.goToReviews}>
          <Button btnStyle={styles.navBtnText}>
            Reviews
          </Button>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.navBtn, this.state.info]} onPress={this.goToInfo}>
          <Button btnStyle={styles.navBtnText}>
            Info
          </Button>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.navBtn, this.state.aboutUs]} onPress={this.goToAboutUs}>
          <Button btnStyle={styles.navBtnText}>
            About Us
          </Button>
        </TouchableOpacity>
      </CardSection>
    );
  }
}
