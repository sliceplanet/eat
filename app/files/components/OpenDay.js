import React from 'react';
import {
  View,
  Image,
  Text,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Platform,
  Dimensions,
} from 'react-native';

import currentDay from "../../store/getCurrentDay";
import styles from '../../files/styles';

const OpenDay = ({day}) => {
    let open_1 = day.Times[0].Open;
    let closed_1 = day.Times[0].Closed;
    let open_2 = day.Times[1].Open;
    let closed_2 = day.Times[1].Closed;
    let schedule_day = '';
    let schedule_time = '';

    if(open_1 && open_2){
        schedule_day = `${day.Key} `;
        schedule_time = `${open_1} - ${closed_1} , ${open_2} - ${closed_2}`
    }else if(!open_1 && open_2){
        schedule_day = `${day.Key} `
        schedule_time=`${open_2} - ${closed_2}`;
    }else{
        schedule_day = `${day.Key} `
        schedule_time=`  Closed`;
    }


    if(currentDay === day.Key){
        return <View style={styles.openingTimes__item}>
            <Text style={{ color: '#04822c', fontWeight: '500', fontSize: 14, fontFamily: 'HindVadodara-Light' }}>{schedule_day}</Text>
            <Text style={{ color: '#04822c', fontWeight: '500', fontSize: 14, fontFamily: 'HindVadodara-Light' }}>{schedule_time}</Text>
          </View>;
    }else{
        return <View style={styles.openingTimes__item}>
            <Text style={{ color: '#535353', fontSize: 14, fontFamily: 'HindVadodara-Light' }}>{schedule_day}</Text>
            <Text style={{ color: '#535353', fontSize: 14, fontFamily: 'HindVadodara-Light' }}>{schedule_time}</Text>
          </View>;
    }

}
export default OpenDay;
