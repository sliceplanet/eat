import React from 'react';
import {
  View,
  Image,
  Text,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Platform,
  Dimensions,
} from 'react-native';

import StarRating from 'react-native-star-rating';
import styles from '../../files/styles';


const OtherPopupRest = ({ name, description, logo, rating, reviews }) => {
  function renderWithCommas () {
      let arr = []
      description.map((item, i)=> {
          arr.push(item.Name)
        })
      return(arr.join(", "));
    }
  return (
    <View style={styles.restDownBlockPopup}>
      <Text style={styles.rdbName}>{name}</Text>
      <Text style={styles.rdbDesc}>{renderWithCommas()}</Text>
      <View style={styles.rdbSRwrapp}>
        <StarRating
          disabled={false}
          emptyStar={'ios-star-outline'}
          fullStar={'ios-star'}
          halfStar={'ios-star-half'}
          iconSet={'Ionicons'}
          maxStars={5}
          disabled
          rating={rating}
          selectedStar={(rating) => this.onStarRatingPress(rating)}
          fullStarColor={'red'}
          starStyle={{ fontSize: 20, height: 25, width: 25 }}
        />
        <Text style={styles.rdbBlueText}>{reviews} reviews</Text>
      </View>
    </View>
  );
};

export { OtherPopupRest };
