import React from 'react';
import {
  View,
  Image,
  Text,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Platform,
  Dimensions,
} from 'react-native';

import styles from '../../files/styles';

const width = Dimensions.get('window').width; //full width
const height = Dimensions.get('window').height; //full height

const Offers = (props) => {

  return (
    <View style={{ backgroundColor: '#f5f5f5', display: 'flex', flexDirection: 'column', width: width, padding: 0 }}>
      <Text style={styles.auHeading}>Offers (deals)</Text>
      <View style={styles.offerBlock}>
        <View style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row' }}>
          <Text style={styles.auProductDishName}>Dish name goes here</Text>
          <Text style={styles.auProductDishName}>CHF 45</Text>
        </View>
        <Text style={styles.offerText}>Dish description goes here and  can run over two or more lines</Text>
        <View style={styles.auProductGreyLine}></View>
        <TouchableOpacity>
          <Text style={styles.auProductViewMenu}>View Menu</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.offerBlock}>
        <View style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'row' }}>
          <Text style={styles.auProductDishName}>Dish name goes here</Text>
          <Text style={styles.auProductDishName}>CHF 45</Text>
        </View>
        <Text style={styles.offerText}>Dish description goes here and  can run over two or more lines</Text>
        <View style={styles.auProductGreyLine}></View>
        <TouchableOpacity>
          <Text style={styles.auProductViewMenu}>View Menu</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export { Offers };
