import React, {Component} from 'react'
// rn comoponents
import {
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Image
} from 'react-native'

// Other components
import {
  CardSection,
  Button,
  Navigation
} from './'

// Data about restoran
import restInfo from '../../data/resturants'
// Rating info
import ratings from '../../data/ratings'

// Stars
import StarRating from 'react-native-star-rating';

// Styles
import styles from '../../files/styles'

// Get full screen width and height
const {width, height} = Dimensions.get('window')

const rating = restInfo.RatingStars.toFixed(1);
const ratingToShow = Math.floor(rating);
const maxRating = 5;
let { Quality, DeliveryTime, Driver} = ratings;

export class Header extends Component {
  renderWithComas = () => {
    let arr = []
    restInfo.CuisineTypes.map((item, i)=>{
      arr.push(item.Name)
    })
    return(arr.join(', '))
  }

  render() {
    return (
      <CardSection style={styles.header}>
        <View>
          <Image source={require("../../files/images/sushi.png")} style={styles.headerImg} />
          <View style={styles.headerWhiteTransparentLineAfter}></View>
          <View style={styles.headerWhiteTransparentLineBefore}></View>
          <View style={styles.headerWhiteTransparentLine}></View>
        </View>
        <View style={styles.headerLogoBack}>
          <Image source={{uri: restInfo.Logo[0].StandardResolutionURL}} style={styles.headerLogo} />
        </View>
        <CardSection style={styles.headerBar}>
          <Image source={require("../../files/images/phone.png")} style={styles.phoneicon} />
          <View style={styles.logoWrapp}>
            <Image source={require("../../files/images/main__logo.png")} style={styles.logo} />
          </View>
          <Text style={styles.language}>EN</Text>
        </CardSection>
        <CardSection style={styles.headerBottom}>
          <Text style={styles.headerRestName}>{restInfo.Name}</Text>
          <Text style={styles.headerRestDescription}>{this.renderWithComas()}</Text>
          <View style={styles.headerStarAndReview}>
            <StarRating
              disabled={false}
              emptyStar={'ios-star-outline'}
              fullStar={'ios-star'}
              halfStar={'ios-star-half'}
              iconSet={'Ionicons'}
              maxStars={5}
              disabled
              rating={Math.floor(Quality)}
              fullStarColor={'red'}
              starStyle={{ fontSize: 20, height: 25, width: 25 }}
              containerStyle={{ width: 125, paddingBottom: 8, paddingTop: 8 }}
            />
          <Text style={styles.headerReview}>{restInfo.NumberOfRatings} reviews</Text>
          </View>
          <TouchableOpacity style={styles.orderNow} onPress={this.props.orderNow}>
            <Button>
              Order Now
            </Button>
          </TouchableOpacity>
        </CardSection>
        <Navigation navigation={this.props.navigation} />
      </CardSection>
    );
  }
}
