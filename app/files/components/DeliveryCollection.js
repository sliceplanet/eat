import React from 'react';
import {
  View,
  Image,
  Text,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Platform,
  Dimensions,
} from 'react-native';

import OpenDay from './OpenDay'

import resturants from '../../data/resturants'
import data from '../../data/menuAndPayment';
import schedule from '../../data/schedule'

import styles from '../../files/styles';


const width = Dimensions.get('window').width; //full width
const height = Dimensions.get('window').height; //full height

const DeliveryCollection = (props) => {
  let { IsCollection, IsDelivery } = resturants;
  let areas = data.Menu.MenuCardDetails.DeliveryAreas;
  return (
    <View style={styles.mapBlock}>
      <View style={styles.delcolInfo}>
        <Text style={styles.mapHeading}>Delivery and collection info</Text>
        <View style={{ display: 'flex', flexDirection: 'row'}}>
          {IsDelivery && <View style={styles.deliverColection__type}>
              <View style={styles.deliverColection__ico}>
                <Image source={require("../images/deliver__green__bike.png")} style={{ height: 24, width: 24, resizeMode: 'contain'}} />
              </View>
              <Text style={styles.deliverColection__name}>Delivery free</Text>
            </View>}
          {IsCollection && <View style={styles.deliverColection__type}>
              <View style={styles.deliverColection__ico}>
                <Image source={require("../images/deliver__green__cash.png")} style={{ height: 24, width: 24, resizeMode: 'contain'}} />
              </View>
              <Text style={styles.deliverColection__name}>Min order £10</Text>
            </View>}
        </View>
      </View>
      <View style={styles.delcolInfo}>
        <Text style={styles.mapHeading}>Delivery areas</Text>
        <View style={{ display: 'flex', flexDirection: 'column'}}>
          <View className="deliver-areas__list">
            {areas.map((item, i) => {
                return <Text key={i} style={{ fontFamily: 'Hind Vadodara', fontSize: 14, color: '#266abd', fontWeight: '300', }}>{item.PostCode} - {item.CityName}</Text>
            })}
          </View>
        </View>
      </View>
      <View style={{ width: '100%', marginTop: 6, marginBottom: 16, backgroundColor: "#333", opacity: 0.12, height: 1}}></View>
      <View style={styles.delcolInfo}>
        <Text style={styles.mapHeading}>Opening Times</Text>
        <View style={{ display: 'flex', flexDirection: 'column'}}>
          <View className="openingTimes__list">
            {schedule.LocalOpeningTimes.map((item,i) => {
                return <OpenDay day={item} key={i}/>
            })}
          </View>
        </View>
      </View>
    </View>
  );
};

export { DeliveryCollection };
