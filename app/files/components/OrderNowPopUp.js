import React, {Component} from 'react'
// rn comoponents
import {
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Dimensions,
  Image,
  ScrollView
} from 'react-native'

// Other components
import {
  CardSection,
  Button,
  TopPopupRest,
  OtherPopupRest,
} from './'

import PopupDialog, {
  DialogTitle,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
  FadeAnimation,
} from 'react-native-popup-dialog';

import data from '../../data/menuAndPayment'
import toprest from '../../data/topRestaurant'

// Styles
import styles from '../../files/styles'

// Get full screen width and height
const {width, height} = Dimensions.get('window')

const slideAnimation = new SlideAnimation({ slideFrom: 'bottom' });

export class OrderNowPopUp extends Component {
  constructor(props){
    super(props)
    this.state = {
      value: '',
      showPostcodeNews: false,
      showPostcodeError: false,
      show: {}
    }
  }

  componentDidMount(){
    this.props.onRef(this)
  }

  componentWillUnMount(){
    this.props.onRef(undefined)
  }

  showPopUp (){
    this.setState({show: {zIndex: 1}})
    this.popupDialog.show()
  }

  sendCode = () => {
    let nearPostCodes = [];
    data.Menu.MenuCardDetails.DeliveryAreas.forEach((item) =>{
      nearPostCodes.push(item.PostCode);
    })
    let postCode = this.state.value;
    this.setState({
      value: '',
    })
    let pattern = /^[0-9]{4}$/;
    if (pattern.test(postCode)) {
      if (nearPostCodes.includes(postCode)) {
        this.setState({
          showPostcodeNews: true,
        })
        setTimeout( () => {
          this.setState({
            showPostcodeNews: false,
          });
          this.popupDialog.dismiss();
         }, 2000);
      } else {
        this.popupDialog.dismiss();
        this.popupDialogRest.show();
      }
    } else {
      this.setState({
        showPostcodeError: true,
      })
      setTimeout( () => {
        this.setState({
          showPostcodeError: false,
        })
       }, 2000);
    }
  };

  render() {
    const topArray = toprest.Restaurants.slice(0,4)
    const bottomArray = toprest.Restaurants.slice(4)
    return (
      <View style={[{position: 'absolute', zIndex: -1}, this.state.show ]}>
        <PopupDialog
          ref={(popupDialog) => { this.popupDialog = popupDialog; }}
          containerStyle={styles.containerPopup}
          dialogStyle={styles.dialogStyle}
          dialogAnimation={slideAnimation}
        >
          <View style={styles.popupStyle}>
            <TouchableOpacity style={styles.wrapPopupCloseIcon} onPress={() => this.popupDialog.dismiss()}>
              <Image source={require('../../files/images/popup__cross.png')} style={styles.popupCloseIcon}/>
            </TouchableOpacity>
            <Text style={styles.popupEP}>Enter Postcode</Text>
            <Text style={styles.popupSI}>See if we deliver where you are</Text>
            <View style={styles.postCodeInputWrap}>
              <TextInput
                style={styles.postCodeInput}
                onChangeText={(value) => this.setState({value})}
                value={this.state.value}
                placeholder={'Enter your postcode'}
                placeholderTextColor='#e6e6e6'
                underlineColorAndroid='transparent'
                ref={this.textInput}
              />
              <TouchableOpacity style={styles.goBtn} onPress={this.sendCode}>
                <Button style={styles.textButton}>
                  Go
                </Button>
              </TouchableOpacity>
            </View>
            <Text style={ (this.state.showPostcodeNews) ? styles.popupNews : styles.hidden } ref={this.successCode}>Great news! This restaurant delivers in your area.</Text>
            <Text style={ (this.state.showPostcodeError) ? styles.postcodeError : styles.hidden }>Incorrect input value.</Text>
          </View>
        </PopupDialog>
        <PopupDialog
          ref={(popupDialogRest) => { this.popupDialogRest = popupDialogRest; }}
          containerStyle={styles.containerPopup}
          dialogStyle={styles.dialogRestStyle}
          dialogAnimation={slideAnimation}
        >
          <ScrollView>
            <View style={styles.popupRestStyle}>
              <TouchableOpacity style={styles.wrapPopupCloseIcon} onPress={() => this.popupDialogRest.dismiss()}>
                <Image source={require('../../files/images/popup__cross.png')} style={styles.popupCloseIcon}/>
              </TouchableOpacity>
              <Text style={styles.popupRestHeader}>Top Rated Restaurant Near You:</Text>
              <View style={styles.popupRestWrapp}>
                {topArray.map((item, i) => {
                return (
                  <TopPopupRest
                    name={item.Name}
                    description={item.CuisineTypes}
                    logo={item.Logo[0].StandardResolutionURL}
                    rating={item.RatingStars}
                    reviews={item.NumberOfRatings}
                    key={i}
                  />
                )
              })}
              </View>
              <Text style={styles.popupRestHeader}>Other Popular Restaurants Near You:</Text>
              <View style={styles.popupRestWrapp}>
                {bottomArray.map((item, i) => {
                    return (
                      <OtherPopupRest
                        name={item.Name}
                        description={item.CuisineTypes}
                        logo={item.Logo[0].StandardResolutionURL}
                        rating={item.RatingStars}
                        reviews={item.NumberOfRatings}
                        key={i}
                      />
                    )
                  })}
              </View>
            </View>
          </ScrollView>
        </PopupDialog>
      </View>
    );
  }
}
