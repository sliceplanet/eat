import React from 'react';
import {
  View,
  Image,
  Text,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Platform,
  Dimensions,
} from 'react-native';

import styles from '../../files/styles';

import { CardSection }  from '../../files/components';


const width = Dimensions.get('window').width; //full width
const height = Dimensions.get('window').height; //full height

const WhatsNew = (props) => {

  return (
    <View style={{ backgroundColor: '#f5f5f5', display: 'flex', flexDirection: 'column', width: width, padding: 0 }}>
      <Text style={styles.auHeading}>What's New?</Text>
      <View style={styles.auProductBlock}>
        <View style={styles.auProductBlockTop}>
          <Image source={require('../images/test4.png')} style={styles.auProductBlockPhoto}/>
        </View>
        <View style={styles.auProductBottomBlock}>
          <Text style={styles.auProductDishName}>Dish name goes here</Text>
          <Text style={styles.auProductDishDesc}>Dish description goes here and can run over two or more lines</Text>
          <View style={styles.auProductGreyLine}></View>
          <TouchableOpacity>
            <Text style={styles.auProductViewMenu}>View Menu</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.auProductBlock}>
        <View style={styles.auProductBlockTop}>
          <Image source={require('../images/test4.png')} style={styles.auProductBlockPhoto}/>
        </View>
        <View style={styles.auProductBottomBlock}>
          <Text style={styles.auProductDishName}>Dish name goes here</Text>
          <Text style={styles.auProductDishDesc}>Dish description goes here and can run over two or more lines</Text>
          <View style={styles.auProductGreyLine}></View>
          <TouchableOpacity>
            <Text style={styles.auProductViewMenu}>View Menu</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export { WhatsNew };
