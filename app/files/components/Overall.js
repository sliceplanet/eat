import React from 'react';
import {
  View,
  Image,
  Text,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Platform,
  Dimensions,
} from 'react-native';

import resturants from '../../data/resturants'
import ratings from '../../data/ratings'
import ratingTypes from '../../data/ratingTypes'


import StarRating from 'react-native-star-rating';
import styles from '../../files/styles';
import * as Progress from 'react-native-progress';


const width = Dimensions.get('window').width; //full width
const height = Dimensions.get('window').height; //full height

const Overall = (props) => {
    const rating = resturants.RatingStars.toFixed(1);
    const ratingToShow = Math.floor(rating);
    const maxRating = 5;
    let { Quality, DeliveryTime, Driver} = ratings;
  return (
    <View style={styles.overall}>
      <Text style={styles.overallHeading}>Overall</Text>
      <View style={styles.forGlobalRating}>
        <StarRating
          disabled={false}
          emptyStar={'ios-star-outline'}
          fullStar={'ios-star'}
          halfStar={'ios-star-half'}
          iconSet={'Ionicons'}
          maxStars={maxRating}
          disabled
          rating={ratingToShow}
          fullStarColor={'red'}
          starStyle={{ fontSize: 29, height: 30, width: 30 }}
          containerStyle={{ width: 125, paddingBottom: 8, paddingTop: 8 }}
        />
        <View style={styles.forGlobalRatingNumbers}>
          <Text style={styles.rating}>{rating}</Text>
          <Text style={styles.maximum}>/{maxRating}</Text>
        </View>
      </View>
      <Text style={styles.basedOn}>ø Based on the last 3 months</Text>
      <Text style={[styles.basedOn, styles.basedOnSmall]}>The overall rating is based on reviews left in the last three months to ensure it reflects the resturant's current perfomance.</Text>
      <View style={styles.QDDwrapp}>
        <Text style={styles.overallThreeHeading}>Quality:</Text>
        <View style={[styles.forGlobalRating, styles.marginBottom16]}>
          <StarRating
            disabled={false}
            emptyStar={'ios-star-outline'}
            fullStar={'ios-star'}
            halfStar={'ios-star-half'}
            iconSet={'Ionicons'}
            maxStars={maxRating}
            disabled
            rating={Math.floor(Quality)}
            fullStarColor={'red'}
            starStyle={{ fontSize: 20, height: 25, width: 25 }}
            containerStyle={{ width: 125, paddingBottom: 8, paddingTop: 8 }}
          />
        <View style={styles.forThreeGlobalRatingNumbers}>
            <Text style={[styles.qddrating, styles.qddratingLeft]}>{Quality.toFixed(1)}</Text>
            <Text style={styles.qddrating}>/{maxRating}</Text>
          </View>
        </View>
        <Text style={styles.overallThreeHeading}>Delivery time:</Text>
        <View style={[styles.forGlobalRating, styles.marginBottom16]}>
          <StarRating
            disabled={false}
            emptyStar={'ios-star-outline'}
            fullStar={'ios-star'}
            halfStar={'ios-star-half'}
            iconSet={'Ionicons'}
            maxStars={maxRating}
            disabled
            rating={Math.floor(DeliveryTime)}
            fullStarColor={'red'}
            starStyle={{ fontSize: 20, height: 25, width: 25 }}
            containerStyle={{ width: 125, paddingBottom: 8, paddingTop: 8 }}
          />
          <View style={styles.forThreeGlobalRatingNumbers}>
            <Text style={[styles.qddrating, styles.qddratingLeft]}>{DeliveryTime.toFixed(1)}</Text>
            <Text style={styles.qddrating}>/{maxRating}</Text>
          </View>
        </View>
        <Text style={styles.overallThreeHeading}>Driver frinedliness:</Text>
        <View style={[styles.forGlobalRating, styles.marginBottom16]}>
          <StarRating
            disabled={false}
            emptyStar={'ios-star-outline'}
            fullStar={'ios-star'}
            halfStar={'ios-star-half'}
            iconSet={'Ionicons'}
            maxStars={maxRating}
            disabled
            rating={Math.floor(Driver)}
            fullStarColor={'red'}
            starStyle={{ fontSize: 20, height: 25, width: 25 }}
            containerStyle={{ width: 125, paddingBottom: 8, paddingTop: 8 }}
          />
          <View style={styles.forThreeGlobalRatingNumbers}>
            <Text style={[styles.qddrating, styles.qddratingLeft]}>{Driver.toFixed(1)}</Text>
            <Text style={styles.qddrating}>/{maxRating}</Text>
          </View>
        </View>
      </View>
      <View style={styles.QDDwrapp}>
        {ratingTypes.map((item, index) => {
           return (
             <View style={[styles.forGlobalRating, styles.forRatingBar]}>
               <StarRating
                 disabled={false}
                 emptyStar={'ios-star-outline'}
                 fullStar={'ios-star'}
                 halfStar={'ios-star-half'}
                 iconSet={'Ionicons'}
                 maxStars={maxRating}
                 disabled
                 rating={item.starAmount}
                 fullStarColor={'red'}
                 starStyle={{ fontSize: 20, height: 25, width: 25 }}
                 containerStyle={{ width: 125, paddingBottom: 8, paddingTop: 8 }}
               />
              <View style={styles.forLineProgressWrapp}>
                <Progress.Bar
                 progress={item.percentage / 100}
                 width={width * 0.5}
                 height={20}
                 borderRadius={0}
                 color={rgb(125, 202, 235)}
                 unfilledColor={rgb(237, 245, 255)}
                 borderWidth={0}
                />
                <Text style={styles.amountPercentForLineProgress}>{item.percentage}%</Text>
              </View>
            </View>
           )
        })}
      </View>
    </View>
  );
};

export { Overall };
