import React from 'react';
import {
  View,
  Image,
  Text,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Platform,
  Dimensions,
} from 'react-native';

import PopupDialog, {
  DialogTitle,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
  FadeAnimation,
} from 'react-native-popup-dialog';

const slideAnimation = new SlideAnimation({ slideFrom: 'bottom' });
const scaleAnimation = new ScaleAnimation();
const fadeAnimation = new FadeAnimation({ animationDuration: 150 });

import styles from '../../files/styles';

const width = Dimensions.get('window').width; //full width
const height = Dimensions.get('window').height; //full height

const AnyAllergy = (props) => {

  return (
    <View style={styles.mapBlock}>
      <Text style={styles.mapHeading}>Any allergy or dietary requirements?</Text>
      <Text style={{ marginBottom: 16, fontFamily: 'HindVadodara-Light', color: '#535353', fontSize: 14 }}>If you have an allergy that could harm your health, or have religious requirements (such as halal or kosher), we strongly advise you to contact the restaurant directly before you place your order.</Text>
      <TouchableOpacity onPress={() => this.popupDialogAllergy.show()}>
        <Text style={{ fontFamily: 'HindVadodara-Medium', color: '#266abd', fontWeight: '500', fontSize: 14  }}>Tell us what your allergies are</Text>
      </TouchableOpacity>
      <PopupDialog
        ref={(popupDialogAllergy) => { this.popupDialogAllergy = popupDialogAllergy; }}
        containerStyle={styles.containerPopup}
        dialogStyle={styles.dialogAllergyStyle}
        dialogAnimation={slideAnimation}
      >
        <View style={styles.popupStyle}>
          <TouchableOpacity style={styles.wrapPopupCloseIcon} onPress={() => this.popupDialogAllergy.dismiss()}>
            <Image source={require('../../files/images/popup__cross.png')} style={styles.popupCloseIcon}/>
          </TouchableOpacity>
          <Text style={styles.popupAllergyHeader}>Any allergy or dietary requirements?</Text>
          <Text style={styles.popupSI}>If you have an allergy that could harm your health, or have religious requirements (such as halal or kosher), we strongly advise you to contact the restaurant directly before you place your order.</Text>
        </View>
      </PopupDialog>
    </View>
  );
};

export { AnyAllergy };
