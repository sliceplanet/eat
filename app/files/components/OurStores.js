import React from 'react';
import {
  View,
  Image,
  Text,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Platform,
  Dimensions,
} from 'react-native';

import styles from '../../files/styles';

const width = Dimensions.get('window').width; //full width
const height = Dimensions.get('window').height; //full height

const OurStores = (props) => {

  return (
    <View style={{ backgroundColor: '#f5f5f5', display: 'flex', flexDirection: 'column', width: width, padding: 0 }}>
      <Text style={styles.auHeading}>Our Stores</Text>
      <View style={styles.storeBlock}>
        <Text style={styles.storeName}>Store Name here</Text>
        <Text style={styles.storeAddress}>Bubentalstrasse 7 , 8304 Wallisellen, Zurich, Switzerland</Text>
      </View>
      <View style={styles.storeBlock}>
        <Text style={styles.storeName}>Store Name here</Text>
        <Text style={styles.storeAddress}>Bubentalstrasse 7 , 8304 Wallisellen, Zurich, Switzerland</Text>
      </View>
      <View style={styles.storeBlock}>
        <Text style={styles.storeName}>Store Name here</Text>
        <Text style={styles.storeAddress}>Bubentalstrasse 7 , 8304 Wallisellen, Zurich, Switzerland</Text>
      </View>
      <View style={styles.storeBlock}>
        <Text style={styles.storeName}>Store Name here</Text>
        <Text style={styles.storeAddress}>Bubentalstrasse 7 , 8304 Wallisellen, Zurich, Switzerland</Text>
      </View>
    </View>
  );
};

export { OurStores };
