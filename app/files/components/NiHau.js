import React from 'react';
import {
  View,
  Image,
  Text,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Platform,
  Dimensions,
} from 'react-native';

import styles from '../../files/styles';
import Swiper from 'react-native-swiper';

import { CardSection }  from '../../files/components';


const width = Dimensions.get('window').width; //full width
const height = Dimensions.get('window').height; //full height

const NiHau = (props) => {

  return (
    <View style={{ backgroundColor: 'white', display: 'flex', flexDirection: 'column', flex: 1, padding: 0 }}>
      <View style={{ display: 'flex', flexDirection: 'column', padding: 8, paddingTop: 20 }}>
        <Text style={styles.niHauHeading}>Ni hau - Welcome - Grüezi - Bonjour</Text>
        <Text style={styles.niHauDesc}>Alle unsere Speisen bereiten wir ganz frisch für unsere Gäste zu. Die Zutaten sind das Geheimnis für beste Resultate. Einige Speisen werden gar nach überlieferten Familien-rezepten zubereitet. In Sachen Sushi sind wir vom Fach und verwöhnen Sie mit eigenen und japanischen traditionellen Versionen.</Text>
      </View>
      <CardSection style={styles.containerSlider}>
        <Swiper style={styles.sliderWrapper} height={240}
          onMomentumScrollEnd={(e, state, context) => console.log('index:', state.index)}
          dot={<View style={{backgroundColor: 'rgba(0,0,0,.2)', width: 5, height: 5, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3}} />}
          activeDot={<View style={{backgroundColor: '#000', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3}} />}
          showsPagination={false} loop>
            <View style={styles.slide}>
              <Image resizeMode='stretch' style={styles.imageSlide} source={require('../../files/images/sushi.png')} />
              <Text style={styles.textSlide}>Dish Name</Text>
            </View>
            <View style={styles.slide}>
              <Image resizeMode='stretch' style={styles.imageSlide} source={require('../../files/images/sushi.png')} />
              <Text style={styles.textSlide}>Dish Name</Text>
            </View>
            <View style={styles.slide}>
              <Image resizeMode='stretch' style={styles.imageSlide} source={require('../../files/images/sushi.png')} />
              <Text style={styles.textSlide}>Dish Name</Text>
            </View>
            <View style={styles.slide}>
              <Image resizeMode='stretch' style={styles.imageSlide} source={require('../../files/images/sushi.png')} />
              <Text style={styles.textSlide}>Dish Name</Text>
            </View>
        </Swiper>
      </CardSection>
    </View>
  );
};

export { NiHau };
