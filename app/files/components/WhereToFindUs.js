import React from 'react';
import {
  View,
  Image,
  Text,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Platform,
  Dimensions,
} from 'react-native';

import resturants from '../../data/resturants'
//import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';

import styles from '../../files/styles';


const width = Dimensions.get('window').width; //full width
const height = Dimensions.get('window').height; //full height

const WhereToFindUs = (props) => {
  let {Address,City,Postcode} = resturants;
  return (
    <View style={styles.mapBlock}>
      <Text style={styles.mapHeading}>Where To Find Us</Text>
      <View style={styles.mapMap}>
        
      </View>
      <View style={styles.mapFooter}>
        <Image source={{ uri: resturants.Logo[0].StandardResolutionURL }} style={styles.mapIcon} />
        <View style={styles.map__info}>
            <Text style={styles.apcInfo}>{Address} , {Postcode} {City}</Text>
            <Text style={styles.ccInfo}>Zurich, Switzerland</Text>
        </View>
      </View>
    </View>
  );
};

export { WhereToFindUs };
