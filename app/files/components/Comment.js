import React from 'react';
import {
  View,
  Image,
  Text,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Platform,
  Dimensions,
} from 'react-native';

import StarRating from 'react-native-star-rating';
import styles from '../../files/styles';


const Comment = ({ textComment, authorComment, dateComment, mark }) => {
  let date = dateComment.split('T')[0];
  let markFloated = Math.floor(mark);
  return (
    <View style={styles.commentWr}>
      <View style={styles.comment}>
            <View style={styles.wrap2}>
              <View style={styles.authorData}>
                <Text style={styles.comment__username}>{authorComment}</Text>
                <Text style={styles.comment__date}>{date}</Text>
              </View>
              <StarRating
                disabled={false}
                emptyStar={'ios-star-outline'}
                fullStar={'ios-star'}
                halfStar={'ios-star-half'}
                iconSet={'Ionicons'}
                maxStars={5}
                disabled
                rating={markFloated}
                fullStarColor={'red'}
                starStyle={{ fontSize: 20, height: 25, width: 25 }}
                containerStyle={{ width: 125, paddingBottom: 8, paddingTop: 8 }}
              />
            <Text style={styles.comment__text}>{textComment}</Text>
            </View>
        </View>
        <View style={styles.horLine}></View>
      </View>
  );
};

export { Comment };
