import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  View,
  Image,
  Text,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Platform,
  Dimensions,
} from 'react-native';

import * as actions from '../../store/actions.js';

import test from '../images/test.png';
import test2 from '../images/test2.png';
import test3 from '../images/test3.png';
import test4 from '../images/test4.png';
import plus_icon from '../images/plus-icon.png';
import angle from '../images/angle.png';
import paralelo from '../images/paralelo.png';
import paralelo_new from '../images/paralelo_new.png';
import sushi from '../images/sushi.png';

import styles from '../styles';

class ProductBlock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isClicked: false,
      title: '1',
      totalPrice: 0,
      description: [],
      totalPriceDropdown: '',
      dataFromDropDown: null
    };
  }

  componentDidMount() {

  }

  render() {

    return (
      <View style={styles.productBlock}>
        <View style={styles.productBlockNamePrice}>
          <Text style={styles.productBlockName}>Product Name</Text>
          <View style={styles.productBlockPriceIcon}>
            <Text style={styles.productBlockPrice}>CHF 4.50</Text>
            <Image source={plus_icon} style={styles.plusIcon}/>
          </View>
        </View>
        <View style={styles.productBlockDescription} >
          <Text style={styles.productBlockDescriptionText}>Description</Text>
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {

  };
};

export default connect(
  mapStateToProps,
  actions
)(ProductBlock);
