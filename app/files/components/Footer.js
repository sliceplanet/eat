import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';


import appstore from '../images/appstore.png';
import gplay from '../images/googleplay.png';
import logo_footer from '../images/logo_footer.png';
import fb from '../images/facebook.png';
import tw from '../images/twitter.png';
import gplus from '../images/gplus.png';
import rss from '../images/rss.png';
import ig from '../images/instagram.png';

import styles from '../../files/styles';


const Footer = () => {

  return (
      <View style={styles.footer}>
        <View style={styles.footerTop}>
          <View style={styles.logoFooter}>
            <Image source={require('../images/logo_footer.png')} style={styles.logo__icon}/>
          </View>
          <View style={styles.apps}>
            <Text style={styles.downloadApps}>Dowload our apps</Text>
            <View style={styles.forIcon}>
              <Image source={require('../images/appstore.png')} style={styles.apps__icon}/>
              <Image source={require('../images/googleplay.png')} style={styles.apps__icon}/>
            </View>
          </View>
          <View style={styles.social}>
            <Image source={require('../images/facebook.png')} style={styles.social_icon }/>
            <Image source={require('../images/twitter.png')} style={styles.social_icon }/>
            <Image source={require('../images/instagram.png')} style={styles.social_icon }/>
          </View>
        </View>
        <View style={styles.footerRainbow}>
          <View style={styles.footerRainbowred}></View>
          <View style={styles.footerRainbowgreen}></View>
          <View style={styles.footerRainbowsalad}></View>
          <View style={styles.footerRainbowblue}></View>
          <View style={styles.footerRainbowlightblue}></View>
          <View style={styles.footerRainbowyellow}></View>
        </View>
        <View style={styles.footerBottom}>
          <Text style={styles.footerBottomText}>Terms & Conditions</Text>
          <Text style={styles.footerBottomText}>Legal Notice</Text>
          <Text style={[styles.footerBottomText, styles.footerBottomTextMarginTop]}>EAT.ch GmbH, Werdstrasse 21, 8004 Zurich / eat.ch GmbH 2007-2018</Text>
        </View>
      </View>
  );
};

export { Footer };
