import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

import styles from '../../files/styles';

// If you need personal style for Button, use btnStyle instead of style <Button btnStyle={YOUR_STYLE}>Button Text</Button>
const Button = ({ onPress, children, btnStyle }) => {
  const { buttonStyle, textStyle } = styles;

  return (
      <Text onPress={onPress} style={ btnStyle || styles.btnTextStyle }>
        {children}
      </Text>
  );
};

export { Button };
