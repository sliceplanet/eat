const menuAndPayment = {
	"Menu": {
		"MenuCardDetails": {
			"MenuCardId": 1515,
			"RestaurantId": 1515,
			"ServiceType": "Delivery",
			"Type": "NoMenuType",
			"Description": "Bei Barbezahlung werden Lunch-Checks akzeptiert.\r\n",
			"DeliveryAreas": [{
				"CityName": "Z\u00fcrich",
				"DeliveryCostAboveThreshold": 0,
				"DeliveryCostBelowThreshold": 0,
				"DeliveryThresholdOrderAmount": 60,
				"DeliveryTurningPoint": -1,
				"PostCode": "8044"
			}],
			"DefaultDeliveryArea": {
				"CityName": "",
				"DeliveryCostAboveThreshold": 0,
				"DeliveryCostBelowThreshold": 0,
				"DeliveryThresholdOrderAmount": 38,
				"DeliveryTurningPoint": -1,
				"PostCode": "default"
			}
		},
		"Categories": [{
			"Id": 12899,
			"Name": "Nigiri Reis mit Fisch, Meeresfr\u00fcchten oder Gem\u00fcse belegt",
			"Description": "",
			"Offer": "NoOffer",
			"MenuItems": [{
				"Name": "Shake Nigiri  (1 St\u00fcck)",
				"MenuNumber": "1",
				"Products": [{
					"ProductId": 106706,
					"Price": 4.5,
					"Name": "Shake Nigiri  (1 St\u00fcck)",
					"Description": "Lachs",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "1",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Maguro Nigiri (1 St\u00fcck)",
				"MenuNumber": "2",
				"Products": [{
					"ProductId": 106707,
					"Price": 4.5,
					"Name": "Maguro Nigiri (1 St\u00fcck)",
					"Description": "Thunfisch",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "2",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Ebi Nigiri (1 St\u00fcck)",
				"MenuNumber": "3",
				"Products": [{
					"ProductId": 106708,
					"Price": 4.5,
					"Name": "Ebi Nigiri (1 St\u00fcck)",
					"Description": "Crevetten",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "3",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Unagi Nigiri (1 St\u00fcck)",
				"MenuNumber": "4",
				"Products": [{
					"ProductId": 106709,
					"Price": 4.5,
					"Name": "Unagi Nigiri (1 St\u00fcck)",
					"Description": "Aal",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "4",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Tobiko Nigiri (1 St\u00fcck)",
				"MenuNumber": "5",
				"Products": [{
					"ProductId": 106710,
					"Price": 4.5,
					"Name": "Tobiko Nigiri (1 St\u00fcck)",
					"Description": "Rogen vom fliegenden Fisch",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "5",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Tamago Omelette (1 St\u00fcck)",
				"MenuNumber": "6",
				"Products": [{
					"ProductId": 106711,
					"Price": 3.5,
					"Name": "Tamago Omelette (1 St\u00fcck)",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "6",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Inari Nigiri (1 St\u00fcck)",
				"MenuNumber": "7",
				"Products": [{
					"ProductId": 106712,
					"Price": 3.5,
					"Name": "Inari Nigiri (1 St\u00fcck)",
					"Description": "S\u00fcssliche Tofutasche",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "7",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Avocado Nigiri (1 St\u00fcck)",
				"MenuNumber": "8",
				"Products": [{
					"ProductId": 106713,
					"Price": 4,
					"Name": "Avocado Nigiri (1 St\u00fcck)",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "8",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}]
		}, {
			"Id": 12900,
			"Name": "Sashimi",
			"Description": "",
			"Offer": "NoOffer",
			"MenuItems": [{
				"Name": "Sashimi Mix (7 St\u00fcck)",
				"MenuNumber": "1",
				"Products": [{
					"ProductId": 106714,
					"Price": 26.5,
					"Name": "Sashimi Mix (7 St\u00fcck)",
					"Description": "Serviert mit Sashimi Lachs und Tuna",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "1",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Sashimi Lachs (4 St\u00fcck)",
				"MenuNumber": "2",
				"Products": [{
					"ProductId": 106715,
					"Price": 16,
					"Name": "Sashimi Lachs (4 St\u00fcck)",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "2",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Sashimi Tuna (4 St\u00fcck)",
				"MenuNumber": "3",
				"Products": [{
					"ProductId": 106716,
					"Price": 16,
					"Name": "Sashimi Tuna (4 St\u00fcck)",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "3",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}]
		}, {
			"Id": 12901,
			"Name": "Uramaki Inside-Out-Rolle: Gef\u00fcllte Reisrolle, divers umh\u00fcllt",
			"Description": "",
			"Offer": "NoOffer",
			"MenuItems": [{
				"Name": "Uramaki California Roll",
				"MenuNumber": "1",
				"Products": [{
					"ProductId": 106717,
					"Price": 8,
					"Name": "Uramaki California Roll",
					"Description": "Surimi, Avocado, Mayonnaise, Tobikko",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "1",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1413161,
						"Name": "4 St\u00fcck",
						"Price": 0,
						"GroupId": 318797
					}, {
						"Id": 1413162,
						"Name": "8 St\u00fcck",
						"Price": 7,
						"GroupId": 318797
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Uramaki Thonmousse",
				"MenuNumber": "2",
				"Products": [{
					"ProductId": 106718,
					"Price": 8,
					"Name": "Uramaki Thonmousse",
					"Description": "Thonmousse, Gurke, Sesam",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "2",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1413163,
						"Name": "4 St\u00fcck",
						"Price": 0,
						"GroupId": 318798
					}, {
						"Id": 1413164,
						"Name": "8 St\u00fcck",
						"Price": 7,
						"GroupId": 318798
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Uramaki Rainbow Lachs",
				"MenuNumber": "3",
				"Products": [{
					"ProductId": 106719,
					"Price": 8.5,
					"Name": "Uramaki Rainbow Lachs",
					"Description": "Lachs, Avocado",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "3",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1413165,
						"Name": "4 St\u00fcck",
						"Price": 0,
						"GroupId": 318799
					}, {
						"Id": 1413166,
						"Name": "8 St\u00fcck",
						"Price": 7.5,
						"GroupId": 318799
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Uramaki \u00a8Rainbow Thuna",
				"MenuNumber": "4",
				"Products": [{
					"ProductId": 106720,
					"Price": 9,
					"Name": "Uramaki \u00a8Rainbow Thuna",
					"Description": "Thunfisch, Avocado",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "4",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1413167,
						"Name": "4 St\u00fcck",
						"Price": 0,
						"GroupId": 318800
					}, {
						"Id": 1413168,
						"Name": "8 St\u00fcck",
						"Price": 8,
						"GroupId": 318800
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Uramaki Tempura Maki",
				"MenuNumber": "5",
				"Products": [{
					"ProductId": 106721,
					"Price": 9,
					"Name": "Uramaki Tempura Maki",
					"Description": "Frittierte Crevetten, Avocado, Mayonnaise, Tobikko",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "5",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1413169,
						"Name": "4 St\u00fcck",
						"Price": 0,
						"GroupId": 318801
					}, {
						"Id": 1413170,
						"Name": "8 St\u00fcck",
						"Price": 8,
						"GroupId": 318801
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Uramaki Spicy Thuna (8 St\u00fcck)",
				"MenuNumber": "7",
				"Products": [{
					"ProductId": 106722,
					"Price": 17,
					"Name": "Uramaki Spicy Thuna (8 St\u00fcck)",
					"Description": "Thunfisch, Chili\u00f6l, Gurke",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "7",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Uramaki Vegi Roll (8 St\u00fcck)",
				"MenuNumber": "8",
				"Products": [{
					"ProductId": 106723,
					"Price": 14,
					"Name": "Uramaki Vegi Roll (8 St\u00fcck)",
					"Description": "Verschiedenes Saisongem\u00fcse",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "8",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Uramaki Alasuka",
				"MenuNumber": "6",
				"Products": [{
					"ProductId": 357967,
					"Price": 7.5,
					"Name": "Uramaki Alasuka",
					"Description": "Lachs, Gurken, Sesam",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "6",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1526464,
						"Name": "4 St\u00fcck",
						"Price": 0,
						"GroupId": 337707
					}, {
						"Id": 1526465,
						"Name": "8 St\u00fcck",
						"Price": 6.5,
						"GroupId": 337707
					}],
					"MealParts": [],
					"Tags": []
				}]
			}]
		}, {
			"Id": 12902,
			"Name": "Hoso Maki",
			"Description": "",
			"Offer": "NoOffer",
			"MenuItems": [{
				"Name": "Shake Maki (6 St\u00fcck)",
				"MenuNumber": "1",
				"Products": [{
					"ProductId": 106724,
					"Price": 9,
					"Name": "Shake Maki (6 St\u00fcck)",
					"Description": "Lachs",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "1",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Tekka Maki (6 St\u00fcck)",
				"MenuNumber": "2",
				"Products": [{
					"ProductId": 106725,
					"Price": 9.5,
					"Name": "Tekka Maki (6 St\u00fcck)",
					"Description": "Thunfisch",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "2",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Shake Avo (6 St\u00fcck)",
				"MenuNumber": "3",
				"Products": [{
					"ProductId": 106726,
					"Price": 9.5,
					"Name": "Shake Avo (6 St\u00fcck)",
					"Description": "Lachs, Avocado",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "3",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Ebi Avo (6 St\u00fcck)",
				"MenuNumber": "4",
				"Products": [{
					"ProductId": 106727,
					"Price": 10.5,
					"Name": "Ebi Avo (6 St\u00fcck)",
					"Description": "Crevetten, Avocado",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "4",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Una Kiu (6 St\u00fcck)",
				"MenuNumber": "5",
				"Products": [{
					"ProductId": 106728,
					"Price": 9,
					"Name": "Una Kiu (6 St\u00fcck)",
					"Description": "Aal, Gurken",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "5",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Avos Maki (6 St\u00fcck)",
				"MenuNumber": "6",
				"Products": [{
					"ProductId": 106729,
					"Price": 9,
					"Name": "Avos Maki (6 St\u00fcck)",
					"Description": "Avocado",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "6",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Oshinko Maki (6 St\u00fcck)",
				"MenuNumber": "7",
				"Products": [{
					"ProductId": 106730,
					"Price": 8,
					"Name": "Oshinko Maki (6 St\u00fcck)",
					"Description": "Gelber Rettich",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "7",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Kappa Maki (6 St\u00fcck)",
				"MenuNumber": "8",
				"Products": [{
					"ProductId": 106731,
					"Price": 8,
					"Name": "Kappa Maki (6 St\u00fcck)",
					"Description": "Gurke",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "8",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}]
		}, {
			"Id": 12903,
			"Name": "Sushi Kombi",
			"Description": "",
			"Offer": "NoOffer",
			"MenuItems": [{
				"Name": "Beniko",
				"MenuNumber": "1",
				"Products": [{
					"ProductId": 106732,
					"Price": 22.5,
					"Name": "Beniko",
					"Description": "4 Nigiri, 6 Hosomai vom Chefkoch zusammengestellt",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "1",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Ume",
				"MenuNumber": "2",
				"Products": [{
					"ProductId": 106733,
					"Price": 28,
					"Name": "Ume",
					"Description": "4 Nigiri, 8 Uramaki vom Chefkoch zusammengestellt",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "2",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Tokyo",
				"MenuNumber": "3",
				"Products": [{
					"ProductId": 106734,
					"Price": 35,
					"Name": "Tokyo",
					"Description": "4 Nigiri, 6 Hosomaki, 8 Uramaki vom Chefkoch zusammengestellt",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "3",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}]
		}, {
			"Id": 12904,
			"Name": "Sushi Platten",
			"Description": "",
			"Offer": "NoOffer",
			"MenuItems": [{
				"Name": "Fun for two",
				"MenuNumber": "1",
				"Products": [{
					"ProductId": 106735,
					"Price": 68,
					"Name": "Fun for two",
					"Description": "8 Nigiri, 6 Hosomaki, 8 Uramaki, 4 Sashimi",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "1",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Kai (f\u00fcr 2 bis 3 Personen)",
				"MenuNumber": "2",
				"Products": [{
					"ProductId": 106736,
					"Price": 88,
					"Name": "Kai (f\u00fcr 2 bis 3 Personen)",
					"Description": "10 Nigiri, 12 Hosomaki, 8 Uramaki, 8 Futomaki",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "2",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Sakura (f\u00fcr 3 bis 4 Personen)",
				"MenuNumber": "3",
				"Products": [{
					"ProductId": 106737,
					"Price": 108,
					"Name": "Sakura (f\u00fcr 3 bis 4 Personen)",
					"Description": "12 Nigiri, 12 Hosomaki, 16 Uramaki, 8 Futomaki",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "3",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}]
		}, {
			"Id": 12905,
			"Name": "Vorspeisen",
			"Description": "",
			"Offer": "NoOffer",
			"MenuItems": [{
				"Name": "Fr\u00fchlingsrolle Vegi (2 St\u00fcck)",
				"MenuNumber": "1",
				"Products": [{
					"ProductId": 106738,
					"Price": 8,
					"Name": "Fr\u00fchlingsrolle Vegi (2 St\u00fcck)",
					"Description": "Gef\u00fcllt mit verschiedenem Saisongem\u00fcse an einer Sweet-Sauer Sauce",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "1",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Knusprige gebackene Wantan (5 St\u00fcck)",
				"MenuNumber": "2",
				"Products": [{
					"ProductId": 106739,
					"Price": 8.5,
					"Name": "Knusprige gebackene Wantan (5 St\u00fcck)",
					"Description": "Chinesische Ravioli gef\u00fcllt mit Schweinshackfleisch an einer Sweet-Chilli Sauce",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "2",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Frittierte Crevetten (5 St\u00fcck)",
				"MenuNumber": "3",
				"Products": [{
					"ProductId": 106740,
					"Price": 10.5,
					"Name": "Frittierte Crevetten (5 St\u00fcck)",
					"Description": "Dazu eine Sweet-Chilli Sauce",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "3",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Hausgemachte gebratene Poulet Gyoza (4 St\u00fcck)",
				"MenuNumber": "4",
				"Products": [{
					"ProductId": 106741,
					"Price": 11.5,
					"Name": "Hausgemachte gebratene Poulet Gyoza (4 St\u00fcck)",
					"Description": "Dazu eine Soja-Sauce",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "4",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Pak Lok Haus Dim-Sum (4 St\u00fcck)",
				"MenuNumber": "5",
				"Products": [{
					"ProductId": 106742,
					"Price": 11.5,
					"Name": "Pak Lok Haus Dim-Sum (4 St\u00fcck)",
					"Description": "Ged\u00e4mpfte Teigtasche, gef\u00fcllt mit Crevetten und dazu eine Soja Sauce",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "5",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Gemischte Vorspeisenplatte",
				"MenuNumber": "6",
				"Products": [{
					"ProductId": 106743,
					"Price": 13.5,
					"Name": "Gemischte Vorspeisenplatte",
					"Description": "Fr\u00fchlingsrolle, Wantan, Crevetten, Pouletfl\u00fcgeli und dazu eine Sweet-Chilli Sauce",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "6",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Pouletfl\u00fcgeli (4 St\u00fcck)",
				"MenuNumber": "7",
				"Products": [{
					"ProductId": 106744,
					"Price": 9.5,
					"Name": "Pouletfl\u00fcgeli (4 St\u00fcck)",
					"Description": "Knusprig gebacken dazu eine Sweet-Chilli Sauce",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "7",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Edamame",
				"MenuNumber": "8",
				"Products": [{
					"ProductId": 106745,
					"Price": 7.5,
					"Name": "Edamame",
					"Description": "Gr\u00fcne Sojabohnen",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "8",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Satay Spiesse (3 St\u00fcck)",
				"MenuNumber": "10",
				"Products": [{
					"ProductId": 106750,
					"Price": 9.5,
					"Name": "Satay Spiesse (3 St\u00fcck)",
					"Description": "Pouletspiesse an einer Erdnusssauce",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "10",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}]
		}, {
			"Id": 43670,
			"Name": "Salate",
			"Description": "",
			"Offer": "NoOffer",
			"MenuItems": [{
				"Name": "Gemischter Salat",
				"MenuNumber": "1",
				"Products": [{
					"ProductId": 106746,
					"Price": 7.5,
					"Name": "Gemischter Salat",
					"Description": "Gr\u00fcner Salat mit Hausdressing",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "1",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Avocado Salat",
				"MenuNumber": "2",
				"Products": [{
					"ProductId": 106747,
					"Price": 10.5,
					"Name": "Avocado Salat",
					"Description": "Gr\u00fcner Salat mit Avocado und Hausdressing",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "2",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Crevetten Salat",
				"MenuNumber": "5",
				"Products": [{
					"ProductId": 106748,
					"Price": 12.5,
					"Name": "Crevetten Salat",
					"Description": "Gr\u00fcner Salat mit Crevetten und Hausdressing",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "5",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Saisonsalat",
				"MenuNumber": "3",
				"Products": [{
					"ProductId": 219797,
					"Price": 9.5,
					"Name": "Saisonsalat",
					"Description": "Salat mit Poulet an Hausdressing",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "3",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Papayasalat vegetarisch",
				"MenuNumber": "4",
				"Products": [{
					"ProductId": 219798,
					"Price": 13.5,
					"Name": "Papayasalat vegetarisch",
					"Description": "Saisonsalat, gr\u00fcne Papayastreifen, Karotten, ger\u00f6stete Erdn\u00fcsse, Schlangenbohnen, Chilli, Cherrytomaten und Thai-Limetten Dressing",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "4",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Papayasalat mit Crevetten",
				"MenuNumber": "6",
				"Products": [{
					"ProductId": 219807,
					"Price": 16.5,
					"Name": "Papayasalat mit Crevetten",
					"Description": "Saisonsalat, gr\u00fcne Papayastreifen, Karotten, ger\u00f6stete Erdn\u00fcsse, Schlangenbohnen, Chilli, Cherrytomaten, Crevetten und Thai-Limetten Dressing",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "6",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}]
		}, {
			"Id": 12906,
			"Name": "Suppen",
			"Description": "",
			"Offer": "NoOffer",
			"MenuItems": [{
				"Name": "Gew\u00fcrz Suppe (scharf)",
				"MenuNumber": "1",
				"Products": [{
					"ProductId": 106751,
					"Price": 6.5,
					"Name": "Gew\u00fcrz Suppe (scharf)",
					"Description": "Scharf & sauer mit Pouletfleisch",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "1",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Ravioli Suppe",
				"MenuNumber": "2",
				"Products": [{
					"ProductId": 106752,
					"Price": 6.5,
					"Name": "Ravioli Suppe",
					"Description": "Chinesische Ravioli, gef\u00fcllt mit Schweinefleisch",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "2",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Glasnudeln Suppe mit Poulet",
				"MenuNumber": "3",
				"Products": [{
					"ProductId": 106753,
					"Price": 6.5,
					"Name": "Glasnudeln Suppe mit Poulet",
					"Description": "Pouletfleisch, Bambussprossen und Fr\u00fchlingszwiebeln",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "3",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Mais-Krabben Suppe",
				"MenuNumber": "4",
				"Products": [{
					"ProductId": 106754,
					"Price": 6.5,
					"Name": "Mais-Krabben Suppe",
					"Description": "Krabbenfleisch, Mais und Fr\u00fchlingszwiebeln",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "4",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Gem\u00fcse Suppe",
				"MenuNumber": "5",
				"Products": [{
					"ProductId": 106755,
					"Price": 6.5,
					"Name": "Gem\u00fcse Suppe",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "5",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Miso Suppe",
				"MenuNumber": "6",
				"Products": [{
					"ProductId": 106756,
					"Price": 6.5,
					"Name": "Miso Suppe",
					"Description": "Japanische Spezialit\u00e4t: Mit Miso-Paste, Lauch und Wakame",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "6",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Tom Ying Kung Suppe (sehr scharf)",
				"MenuNumber": "7",
				"Products": [{
					"ProductId": 106757,
					"Price": 11.5,
					"Name": "Tom Ying Kung Suppe (sehr scharf)",
					"Description": "Suppe mit Crevetten und Tomyamgong-Paste",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "7",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Tom Kha Gai Suppe ",
				"MenuNumber": "8",
				"Products": [{
					"ProductId": 106758,
					"Price": 10.5,
					"Name": "Tom Kha Gai Suppe ",
					"Description": "Suppe mit Pouletfleisch und Kokosmilch",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "8",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 208545,
						"Name": "als Vorspeise ohne Reis",
						"Price": 0,
						"GroupId": 73358
					}, {
						"Id": 208546,
						"Name": "als Hauptspeise mit Reis",
						"Price": 12,
						"GroupId": 73358
					}],
					"MealParts": [],
					"Tags": []
				}]
			}]
		}, {
			"Id": 53722,
			"Name": "Thai Specials",
			"Description": "",
			"Offer": "NoOffer",
			"MenuItems": [{
				"Name": "Gr\u00fcnes Thai Curry (scharf) Jasminreis",
				"MenuNumber": "1",
				"Products": [{
					"ProductId": 440001,
					"Price": 20.5,
					"Name": "Gr\u00fcnes Thai Curry (scharf) Jasminreis",
					"Description": "Gem\u00fcse, Kokosmilch, gr\u00fcne Currypaste",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "1",
					"Offer": "NoOffer",
					"OptionalAccessories": [{
						"Id": 2072859,
						"Name": "Gem\u00fcse",
						"Price": 0,
						"GroupId": 429522
					}, {
						"Id": 2072860,
						"Name": "Tofu",
						"Price": 3,
						"GroupId": 429522
					}, {
						"Id": 2072861,
						"Name": "Poulet",
						"Price": 3,
						"GroupId": 429522
					}, {
						"Id": 2072862,
						"Name": "Rindfleisch (CH)",
						"Price": 5.5,
						"GroupId": 429522
					}, {
						"Id": 2072863,
						"Name": "Fisch",
						"Price": 5.5,
						"GroupId": 429522
					}, {
						"Id": 2072864,
						"Name": "Crevetten",
						"Price": 8,
						"GroupId": 429522
					}, {
						"Id": 2072865,
						"Name": "Ente knusprig",
						"Price": 8,
						"GroupId": 429522
					}],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Rotes Thai Curry (leicht scharf)  Jasminreis",
				"MenuNumber": "4",
				"Products": [{
					"ProductId": 440006,
					"Price": 20.5,
					"Name": "Rotes Thai Curry (leicht scharf)  Jasminreis",
					"Description": "Gem\u00fcse, Kokosmilch, rote Currypaste",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "4",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 2072866,
						"Name": "Gem\u00fcse",
						"Price": 0,
						"GroupId": 429527
					}, {
						"Id": 2072867,
						"Name": "Tofu",
						"Price": 3,
						"GroupId": 429527
					}, {
						"Id": 2072868,
						"Name": "Poulet",
						"Price": 3,
						"GroupId": 429527
					}, {
						"Id": 2072869,
						"Name": "Rindfleisch (CH)",
						"Price": 5.5,
						"GroupId": 429527
					}, {
						"Id": 2072870,
						"Name": "Fisch",
						"Price": 5.5,
						"GroupId": 429527
					}, {
						"Id": 2072871,
						"Name": "Crevetten",
						"Price": 8,
						"GroupId": 429527
					}, {
						"Id": 2072872,
						"Name": "Ente knusprig",
						"Price": 8,
						"GroupId": 429527
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Paneng Curry (mild scharf)  Jasminreis",
				"MenuNumber": "7",
				"Products": [{
					"ProductId": 440012,
					"Price": 20.5,
					"Name": "Paneng Curry (mild scharf)  Jasminreis",
					"Description": "Thai Kokos-Curry mit Zitronenbl\u00e4ttern",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "7",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 2072873,
						"Name": "Gem\u00fcse",
						"Price": 0,
						"GroupId": 429533
					}, {
						"Id": 2072874,
						"Name": "Tofu",
						"Price": 3,
						"GroupId": 429533
					}, {
						"Id": 2072875,
						"Name": "Poulet",
						"Price": 3,
						"GroupId": 429533
					}, {
						"Id": 2072876,
						"Name": "Rindfleisch (CH)",
						"Price": 5.5,
						"GroupId": 429533
					}, {
						"Id": 2072877,
						"Name": "Fisch",
						"Price": 5.5,
						"GroupId": 429533
					}, {
						"Id": 2072878,
						"Name": "Crevetten",
						"Price": 8,
						"GroupId": 429533
					}, {
						"Id": 2072879,
						"Name": "Ente knusprig",
						"Price": 8,
						"GroupId": 429533
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Gelbes Curry (nicht scharf) Jasminreis",
				"MenuNumber": "10",
				"Products": [{
					"ProductId": 440018,
					"Price": 20.5,
					"Name": "Gelbes Curry (nicht scharf) Jasminreis",
					"Description": "Gebratene Zutaten mit Thai Basilikum",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "10",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 2072880,
						"Name": "Gem\u00fcse",
						"Price": 0,
						"GroupId": 429539
					}, {
						"Id": 2072881,
						"Name": "Tofu",
						"Price": 3,
						"GroupId": 429539
					}, {
						"Id": 2072882,
						"Name": "Poulet",
						"Price": 3,
						"GroupId": 429539
					}, {
						"Id": 2072883,
						"Name": "Rindfleisch (CH)",
						"Price": 5.5,
						"GroupId": 429539
					}, {
						"Id": 2072884,
						"Name": "Fisch",
						"Price": 5.5,
						"GroupId": 429539
					}, {
						"Id": 2072885,
						"Name": "Crevetten",
						"Price": 8,
						"GroupId": 429539
					}, {
						"Id": 2072886,
						"Name": "Ente knusprig",
						"Price": 8,
						"GroupId": 429539
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Pad Grapao (sehr scharf) Jasminreis",
				"MenuNumber": "13",
				"Products": [{
					"ProductId": 440024,
					"Price": 20.5,
					"Name": "Pad Grapao (sehr scharf) Jasminreis",
					"Description": "Gebratene Zutaten mit Thai Basilikum",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "13",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 2072852,
						"Name": "Gem\u00fcse",
						"Price": 0,
						"GroupId": 429545
					}, {
						"Id": 2072853,
						"Name": "Tofu",
						"Price": 3,
						"GroupId": 429545
					}, {
						"Id": 2072854,
						"Name": "Poulet",
						"Price": 3,
						"GroupId": 429545
					}, {
						"Id": 2072855,
						"Name": "Rindfleisch (CH)",
						"Price": 5.5,
						"GroupId": 429545
					}, {
						"Id": 2072856,
						"Name": "Fisch",
						"Price": 5.5,
						"GroupId": 429545
					}, {
						"Id": 2072857,
						"Name": "Crevetten",
						"Price": 8,
						"GroupId": 429545
					}, {
						"Id": 2072858,
						"Name": "Ente knusprig",
						"Price": 8,
						"GroupId": 429545
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Gr\u00fcnes Thai Curry (scharf) gebratener Reis",
				"MenuNumber": "2",
				"Products": [{
					"ProductId": 445744,
					"Price": 23.5,
					"Name": "Gr\u00fcnes Thai Curry (scharf) gebratener Reis",
					"Description": "Gem\u00fcse, Kokosmilch, gr\u00fcne Currypaste",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "2",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 2072887,
						"Name": "Gem\u00fcse",
						"Price": 0,
						"GroupId": 435787
					}, {
						"Id": 2072888,
						"Name": "Tofu",
						"Price": 3,
						"GroupId": 435787
					}, {
						"Id": 2072889,
						"Name": "Poulet",
						"Price": 3,
						"GroupId": 435787
					}, {
						"Id": 2072890,
						"Name": "Rindfleisch (CH)",
						"Price": 5.5,
						"GroupId": 435787
					}, {
						"Id": 2072891,
						"Name": "Fisch",
						"Price": 5.5,
						"GroupId": 435787
					}, {
						"Id": 2072892,
						"Name": "Crevetten",
						"Price": 8,
						"GroupId": 435787
					}, {
						"Id": 2072893,
						"Name": "Ente knusprig",
						"Price": 8,
						"GroupId": 435787
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Gr\u00fcnes Thai Curry (scharf) gebratene Nudeln",
				"MenuNumber": "3",
				"Products": [{
					"ProductId": 445745,
					"Price": 23.5,
					"Name": "Gr\u00fcnes Thai Curry (scharf) gebratene Nudeln",
					"Description": "Gem\u00fcse, Kokosmilch, gr\u00fcne Currypaste",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "3",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 2072894,
						"Name": "Gem\u00fcse",
						"Price": 0,
						"GroupId": 435788
					}, {
						"Id": 2072895,
						"Name": "Tofu",
						"Price": 3,
						"GroupId": 435788
					}, {
						"Id": 2072896,
						"Name": "Poulet",
						"Price": 3,
						"GroupId": 435788
					}, {
						"Id": 2072897,
						"Name": "Rindfleisch (CH)",
						"Price": 5.5,
						"GroupId": 435788
					}, {
						"Id": 2072898,
						"Name": "Fisch",
						"Price": 5.5,
						"GroupId": 435788
					}, {
						"Id": 2072899,
						"Name": "Crevetten",
						"Price": 8,
						"GroupId": 435788
					}, {
						"Id": 2072900,
						"Name": "Ente knusprig",
						"Price": 8,
						"GroupId": 435788
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Rotes Thai Curry (leicht scharf) gebratener Reis",
				"MenuNumber": "5",
				"Products": [{
					"ProductId": 445746,
					"Price": 23.5,
					"Name": "Rotes Thai Curry (leicht scharf) gebratener Reis",
					"Description": "Gem\u00fcse, Kokosmilch, rote Currypaste",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "5",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 2072901,
						"Name": "Gem\u00fcse",
						"Price": 0,
						"GroupId": 435789
					}, {
						"Id": 2072902,
						"Name": "Tofu",
						"Price": 3,
						"GroupId": 435789
					}, {
						"Id": 2072903,
						"Name": "Poulet",
						"Price": 3,
						"GroupId": 435789
					}, {
						"Id": 2072904,
						"Name": "Rindfleisch (CH)",
						"Price": 5.5,
						"GroupId": 435789
					}, {
						"Id": 2072905,
						"Name": "Fisch",
						"Price": 5.5,
						"GroupId": 435789
					}, {
						"Id": 2072906,
						"Name": "Crevetten",
						"Price": 8,
						"GroupId": 435789
					}, {
						"Id": 2072907,
						"Name": "Ente knusprig",
						"Price": 8,
						"GroupId": 435789
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Rotes Thai Curry (leicht scharf) gebratene Nudeln",
				"MenuNumber": "6",
				"Products": [{
					"ProductId": 445747,
					"Price": 23.5,
					"Name": "Rotes Thai Curry (leicht scharf) gebratene Nudeln",
					"Description": "Gem\u00fcse, Kokosmilch, rote Currypaste",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "6",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 2072908,
						"Name": "Gem\u00fcse",
						"Price": 0,
						"GroupId": 435790
					}, {
						"Id": 2072909,
						"Name": "Tofu",
						"Price": 3,
						"GroupId": 435790
					}, {
						"Id": 2072910,
						"Name": "Poulet",
						"Price": 3,
						"GroupId": 435790
					}, {
						"Id": 2072911,
						"Name": "Rindfleisch (CH)",
						"Price": 5.5,
						"GroupId": 435790
					}, {
						"Id": 2072912,
						"Name": "Fisch",
						"Price": 5.5,
						"GroupId": 435790
					}, {
						"Id": 2072913,
						"Name": "Crevetten",
						"Price": 8,
						"GroupId": 435790
					}, {
						"Id": 2072914,
						"Name": "Ente knusprig",
						"Price": 8,
						"GroupId": 435790
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Paneng Curry (mild scharf) gebratener Reis",
				"MenuNumber": "8",
				"Products": [{
					"ProductId": 445748,
					"Price": 23.5,
					"Name": "Paneng Curry (mild scharf) gebratener Reis",
					"Description": "Thai Kokos-Curry mit Zitronenbl\u00e4ttern",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "8",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 2072915,
						"Name": "Gem\u00fcse",
						"Price": 0,
						"GroupId": 435791
					}, {
						"Id": 2072916,
						"Name": "Tofu",
						"Price": 3,
						"GroupId": 435791
					}, {
						"Id": 2072917,
						"Name": "Poulet",
						"Price": 3,
						"GroupId": 435791
					}, {
						"Id": 2072918,
						"Name": "Rindfleisch (CH)",
						"Price": 5.5,
						"GroupId": 435791
					}, {
						"Id": 2072919,
						"Name": "Fisch",
						"Price": 5.5,
						"GroupId": 435791
					}, {
						"Id": 2072920,
						"Name": "Crevetten",
						"Price": 8,
						"GroupId": 435791
					}, {
						"Id": 2072921,
						"Name": "Ente knusprig",
						"Price": 8,
						"GroupId": 435791
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Paneng Curry (mild scharf) gebratene Nudeln",
				"MenuNumber": "9",
				"Products": [{
					"ProductId": 445749,
					"Price": 23.5,
					"Name": "Paneng Curry (mild scharf) gebratene Nudeln",
					"Description": "Thai Kokos-Curry mit Zitronenbl\u00e4ttern",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "9",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 2072922,
						"Name": "Gem\u00fcse",
						"Price": 0,
						"GroupId": 435792
					}, {
						"Id": 2072923,
						"Name": "Tofu",
						"Price": 3,
						"GroupId": 435792
					}, {
						"Id": 2072924,
						"Name": "Poulet",
						"Price": 3,
						"GroupId": 435792
					}, {
						"Id": 2072925,
						"Name": "Rindfleisch (CH)",
						"Price": 5.5,
						"GroupId": 435792
					}, {
						"Id": 2072926,
						"Name": "Fisch",
						"Price": 5.5,
						"GroupId": 435792
					}, {
						"Id": 2072927,
						"Name": "Crevetten",
						"Price": 8,
						"GroupId": 435792
					}, {
						"Id": 2072928,
						"Name": "Ente knusprig",
						"Price": 8,
						"GroupId": 435792
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Gelbes Curry (nicht scharf) gebratener Reis",
				"MenuNumber": "11",
				"Products": [{
					"ProductId": 445750,
					"Price": 23.5,
					"Name": "Gelbes Curry (nicht scharf) gebratener Reis",
					"Description": "Gebratene Zutaten mit Thai Basilikum",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "11",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 2072929,
						"Name": "Gem\u00fcse",
						"Price": 0,
						"GroupId": 435793
					}, {
						"Id": 2072930,
						"Name": "Tofu",
						"Price": 3,
						"GroupId": 435793
					}, {
						"Id": 2072931,
						"Name": "Poulet",
						"Price": 3,
						"GroupId": 435793
					}, {
						"Id": 2072932,
						"Name": "Rindfleisch (CH)",
						"Price": 5.5,
						"GroupId": 435793
					}, {
						"Id": 2072933,
						"Name": "Fisch",
						"Price": 5.5,
						"GroupId": 435793
					}, {
						"Id": 2072934,
						"Name": "Crevetten",
						"Price": 8,
						"GroupId": 435793
					}, {
						"Id": 2072935,
						"Name": "Ente knusprig",
						"Price": 8,
						"GroupId": 435793
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Gelbes Gelbes Curry (nicht scharf) gebratene Nudeln",
				"MenuNumber": "12",
				"Products": [{
					"ProductId": 445751,
					"Price": 23.5,
					"Name": "Gelbes Gelbes Curry (nicht scharf) gebratene Nudeln",
					"Description": "Gebratene Zutaten mit Thai Basilikum",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "12",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 2072936,
						"Name": "Gem\u00fcse",
						"Price": 0,
						"GroupId": 435794
					}, {
						"Id": 2072937,
						"Name": "Tofu",
						"Price": 3,
						"GroupId": 435794
					}, {
						"Id": 2072938,
						"Name": "Poulet",
						"Price": 3,
						"GroupId": 435794
					}, {
						"Id": 2072939,
						"Name": "Rindfleisch (CH)",
						"Price": 5.5,
						"GroupId": 435794
					}, {
						"Id": 2072940,
						"Name": "Fisch",
						"Price": 5.5,
						"GroupId": 435794
					}, {
						"Id": 2072941,
						"Name": "Crevetten",
						"Price": 8,
						"GroupId": 435794
					}, {
						"Id": 2072942,
						"Name": "Ente knusprig",
						"Price": 8,
						"GroupId": 435794
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Pad Grapao (sehr scharf) gebratener Reis",
				"MenuNumber": "13",
				"Products": [{
					"ProductId": 445752,
					"Price": 23.5,
					"Name": "Pad Grapao (sehr scharf) gebratener Reis",
					"Description": "Gebratene Zutaten mit Thai Basilikum",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "13",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 2072943,
						"Name": "Gem\u00fcse",
						"Price": 0,
						"GroupId": 435795
					}, {
						"Id": 2072944,
						"Name": "Tofu",
						"Price": 3,
						"GroupId": 435795
					}, {
						"Id": 2072945,
						"Name": "Poulet",
						"Price": 3,
						"GroupId": 435795
					}, {
						"Id": 2072946,
						"Name": "Rindfleisch (CH)",
						"Price": 5.5,
						"GroupId": 435795
					}, {
						"Id": 2072947,
						"Name": "Fisch",
						"Price": 5.5,
						"GroupId": 435795
					}, {
						"Id": 2072948,
						"Name": "Crevetten",
						"Price": 8,
						"GroupId": 435795
					}, {
						"Id": 2072949,
						"Name": "Ente knusprig",
						"Price": 8,
						"GroupId": 435795
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Pad Grapao (sehr scharf) gebratene Nudeln",
				"MenuNumber": "14",
				"Products": [{
					"ProductId": 445753,
					"Price": 23.5,
					"Name": "Pad Grapao (sehr scharf) gebratene Nudeln",
					"Description": "Gebratene Zutaten mit Thai Basilikum",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "14",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 2072950,
						"Name": "Gem\u00fcse",
						"Price": 0,
						"GroupId": 435796
					}, {
						"Id": 2072951,
						"Name": "Tofu",
						"Price": 3,
						"GroupId": 435796
					}, {
						"Id": 2072952,
						"Name": "Poulet",
						"Price": 3,
						"GroupId": 435796
					}, {
						"Id": 2072953,
						"Name": "Rindfleisch (CH)",
						"Price": 5.5,
						"GroupId": 435796
					}, {
						"Id": 2072954,
						"Name": "Fisch",
						"Price": 5.5,
						"GroupId": 435796
					}, {
						"Id": 2072955,
						"Name": "Crevetten",
						"Price": 8,
						"GroupId": 435796
					}, {
						"Id": 2072956,
						"Name": "Ente knusprig",
						"Price": 8,
						"GroupId": 435796
					}],
					"MealParts": [],
					"Tags": []
				}]
			}]
		}, {
			"Id": 12907,
			"Name": "Hausspezialit\u00e4ten",
			"Description": "",
			"Offer": "NoOffer",
			"MenuItems": [{
				"Name": "Schweinefleisch mit Gew\u00fcrzen (scharf)",
				"MenuNumber": "1",
				"Products": [{
					"ProductId": 106759,
					"Price": 29.5,
					"Name": "Schweinefleisch mit Gew\u00fcrzen (scharf)",
					"Description": "In Streifen geschnittenes Schweinefleisch mit Ingwer, Knoblauch, Lauch Sauce und dreierlei Gew\u00fcrzen",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "1",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1542021,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73359
					}, {
						"Id": 1542022,
						"Name": "gebratenem Reis",
						"Price": 2.5,
						"GroupId": 73359
					}, {
						"Id": 1542023,
						"Name": "gebratenen Nudeln",
						"Price": 2.5,
						"GroupId": 73359
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Schweinebraten mit Wok-Gem\u00fcse",
				"MenuNumber": "2",
				"Products": [{
					"ProductId": 106760,
					"Price": 27.5,
					"Name": "Schweinebraten mit Wok-Gem\u00fcse",
					"Description": "Grillierter und marinierter Schweinebraten serviert mit Wok-Gem\u00fcse",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "2",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1552738,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73360
					}, {
						"Id": 1552739,
						"Name": "gebratenem Reis",
						"Price": 2.5,
						"GroupId": 73360
					}, {
						"Id": 1552740,
						"Name": "gebratenen Nudeln",
						"Price": 2.5,
						"GroupId": 73360
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Rindfleisch-Streifen an s\u00fcss-scharfer Sauce",
				"MenuNumber": "3",
				"Products": [{
					"ProductId": 106761,
					"Price": 23.5,
					"Name": "Rindfleisch-Streifen an s\u00fcss-scharfer Sauce",
					"Description": "gebackene Rindfleisch-Streifen an s\u00fcss-scharfer Sauce",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "3",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1879805,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73361
					}, {
						"Id": 1879806,
						"Name": "gebratenem Reis",
						"Price": 2,
						"GroupId": 73361
					}, {
						"Id": 1879807,
						"Name": "gebratenen Nudeln",
						"Price": 2,
						"GroupId": 73361
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Schweinefleisch S\u00fcss-Sauer",
				"MenuNumber": "4",
				"Products": [{
					"ProductId": 106762,
					"Price": 24,
					"Name": "Schweinefleisch S\u00fcss-Sauer",
					"Description": "Teigtaschen gef\u00fcllt mit Schweinfleisch an einer S\u00fcss-Sauer Sauce",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "4",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1542030,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73362
					}, {
						"Id": 1542031,
						"Name": "gebratenem Reis",
						"Price": 2.5,
						"GroupId": 73362
					}, {
						"Id": 1542032,
						"Name": "gebratenen Nudeln",
						"Price": 2.5,
						"GroupId": 73362
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Gebratenes Poulet mit japanischem Teriyaki",
				"MenuNumber": "5",
				"Products": [{
					"ProductId": 106763,
					"Price": 31,
					"Name": "Gebratenes Poulet mit japanischem Teriyaki",
					"Description": "Gebratenes Poulet mit japanischer Teriyaki Sauce serviert mit Wok-Gem\u00fcse",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "5",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1552741,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73363
					}, {
						"Id": 1552742,
						"Name": "gebratenem Reis",
						"Price": 2.5,
						"GroupId": 73363
					}, {
						"Id": 1552743,
						"Name": "gebratenen Nudeln",
						"Price": 2.5,
						"GroupId": 73363
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Gebratener Lachs mit japanischem Teriyaki",
				"MenuNumber": "6",
				"Products": [{
					"ProductId": 106764,
					"Price": 34,
					"Name": "Gebratener Lachs mit japanischem Teriyaki",
					"Description": "Gebratener Lachs mit japanischer Teriyaki Sauce serviert mit Wok-Gem\u00fcse",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "6",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1552744,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73364
					}, {
						"Id": 1552745,
						"Name": "gebratenem Reis",
						"Price": 2.5,
						"GroupId": 73364
					}, {
						"Id": 1552746,
						"Name": "gebratenen Nudeln",
						"Price": 2.5,
						"GroupId": 73364
					}],
					"MealParts": [],
					"Tags": []
				}]
			}]
		}, {
			"Id": 12908,
			"Name": "Pouletgerichte",
			"Description": "",
			"Offer": "NoOffer",
			"MenuItems": [{
				"Name": "Poulet S\u00fcss Sauer mit Teig",
				"MenuNumber": "3",
				"Products": [{
					"ProductId": 106765,
					"Price": 23,
					"Name": "Poulet S\u00fcss Sauer mit Teig",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "3",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 397245,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73365
					}, {
						"Id": 397246,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73365
					}, {
						"Id": 397247,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73365
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Poulet Szechuan (scharf)",
				"MenuNumber": "4",
				"Products": [{
					"ProductId": 106766,
					"Price": 23,
					"Name": "Poulet Szechuan (scharf)",
					"Description": "Pouletfleisch und Saisongem\u00fcse nach Szechuan Art",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "4",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 397248,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73366
					}, {
						"Id": 397249,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73366
					}, {
						"Id": 397250,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73366
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Poulet KungBao (mild und sauer)",
				"MenuNumber": "5",
				"Products": [{
					"ProductId": 106767,
					"Price": 24,
					"Name": "Poulet KungBao (mild und sauer)",
					"Description": "Pouletfleisch, Kerneln\u00fcssen und Saisongem\u00fcse an einer dunklen Sauce",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "5",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 397254,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73367
					}, {
						"Id": 397255,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73367
					}, {
						"Id": 397256,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73367
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Poulet an Sataysauce",
				"MenuNumber": "6",
				"Products": [{
					"ProductId": 106768,
					"Price": 23,
					"Name": "Poulet an Sataysauce",
					"Description": "Pouletfleisch und Saisongem\u00fcse nach PakLok Art an einer Sataysauce",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "6",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1653000,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73368
					}, {
						"Id": 1653001,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73368
					}, {
						"Id": 1653002,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73368
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Poulet mit verschiedenem Saisongem\u00fcse",
				"MenuNumber": "7",
				"Products": [{
					"ProductId": 106769,
					"Price": 23,
					"Name": "Poulet mit verschiedenem Saisongem\u00fcse",
					"Description": "Poulet Gem\u00fcse, mit Soja-Sauce gebraten",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "7",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1552699,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73369
					}, {
						"Id": 1552700,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73369
					}, {
						"Id": 1552701,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73369
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Poulet mit chinesischen Pilzen und Bambussprossen",
				"MenuNumber": "8",
				"Products": [{
					"ProductId": 106770,
					"Price": 24,
					"Name": "Poulet mit chinesischen Pilzen und Bambussprossen",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "8",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 397269,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73370
					}, {
						"Id": 397270,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73370
					}, {
						"Id": 397271,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73370
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Klein Poulet S\u00fcss-Sauer im Teig",
				"MenuNumber": "1",
				"Products": [{
					"ProductId": 389960,
					"Price": 19.5,
					"Name": "Klein Poulet S\u00fcss-Sauer im Teig",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "1",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1713100,
						"Name": "Jasminreis",
						"Price": 0,
						"GroupId": 373051
					}, {
						"Id": 1713101,
						"Name": "gebratener Reis",
						"Price": 3,
						"GroupId": 373051
					}, {
						"Id": 1713102,
						"Name": "gebratene Nudeln",
						"Price": 3,
						"GroupId": 373051
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Klein Poulet Szechuan",
				"MenuNumber": "2",
				"Products": [{
					"ProductId": 389961,
					"Price": 19.5,
					"Name": "Klein Poulet Szechuan",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "2",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1713103,
						"Name": "Jasminreis",
						"Price": 0,
						"GroupId": 373052
					}, {
						"Id": 1713104,
						"Name": "gebratener Reis",
						"Price": 3,
						"GroupId": 373052
					}, {
						"Id": 1713105,
						"Name": "gebratene Nudeln",
						"Price": 3,
						"GroupId": 373052
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Poulet Gr\u00fcnes Curry (scharf)",
				"MenuNumber": "9",
				"Products": [{
					"ProductId": 439996,
					"Price": 23.5,
					"Name": "Poulet Gr\u00fcnes Curry (scharf)",
					"Description": "Gem\u00fcse, Kokosmilch, gr\u00fcne Currypaste, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "9",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Poulet Rotes Thai Curry (leicht scharf)",
				"MenuNumber": "10",
				"Products": [{
					"ProductId": 440002,
					"Price": 23.5,
					"Name": "Poulet Rotes Thai Curry (leicht scharf)",
					"Description": "Gem\u00fcse, Kokosmilch, rote Currypaste, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "10",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Poulet Paneng Curry (mild scharf)",
				"MenuNumber": "11",
				"Products": [{
					"ProductId": 440008,
					"Price": 23.5,
					"Name": "Poulet Paneng Curry (mild scharf)",
					"Description": "Thai Kokos-Curry mit Zitronenbl\u00e4ttern, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "11",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Poulet Gelbes Curry (nicht scharf)",
				"MenuNumber": "12",
				"Products": [{
					"ProductId": 440014,
					"Price": 23.5,
					"Name": "Poulet Gelbes Curry (nicht scharf)",
					"Description": "Gem\u00fcse, Kokosmilch, gelbe Currypaste, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "12",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Poulet Pad Grapao (sehr scharf)",
				"MenuNumber": "13",
				"Products": [{
					"ProductId": 440020,
					"Price": 23.5,
					"Name": "Poulet Pad Grapao (sehr scharf)",
					"Description": "Gebratene Zutaten mit Thai Basilikum, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "13",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}]
		}, {
			"Id": 12909,
			"Name": "Fischgerichte",
			"Description": "",
			"Offer": "NoOffer",
			"MenuItems": [{
				"Name": "Gebackener Fisch S\u00fcss Sauer mit Teig",
				"MenuNumber": "1",
				"Products": [{
					"ProductId": 106771,
					"Price": 26,
					"Name": "Gebackener Fisch S\u00fcss Sauer mit Teig",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "1",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 397272,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73371
					}, {
						"Id": 397273,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73371
					}, {
						"Id": 397274,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73371
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Gebackener Fisch Szechuan Art",
				"MenuNumber": "2",
				"Products": [{
					"ProductId": 106772,
					"Price": 25,
					"Name": "Gebackener Fisch Szechuan Art",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "2",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 397275,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73372
					}, {
						"Id": 397276,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73372
					}, {
						"Id": 397277,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73372
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Gebackener Fisch mit Ingwer & Fr\u00fchlingszwiebeln",
				"MenuNumber": "3",
				"Products": [{
					"ProductId": 106773,
					"Price": 25,
					"Name": "Gebackener Fisch mit Ingwer & Fr\u00fchlingszwiebeln",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "3",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 397278,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73373
					}, {
						"Id": 397279,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73373
					}, {
						"Id": 397280,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73373
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Gebackener Fisch mit verschiedenem Saisongem\u00fcse",
				"MenuNumber": "4",
				"Products": [{
					"ProductId": 106774,
					"Price": 25,
					"Name": "Gebackener Fisch mit verschiedenem Saisongem\u00fcse",
					"Description": "Fisch Gem\u00fcse, mit Soja-Sauce gebraten",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "4",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1552732,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73374
					}, {
						"Id": 1552733,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73374
					}, {
						"Id": 1552734,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73374
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Fisch Gr\u00fcnes Thai Curry (scharf)",
				"MenuNumber": "5",
				"Products": [{
					"ProductId": 439998,
					"Price": 25.5,
					"Name": "Fisch Gr\u00fcnes Thai Curry (scharf)",
					"Description": "Gem\u00fcse, Kokosmilch, gr\u00fcne Currypaste, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "5",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Fisch Rotes Thai Curry (leicht scharf)",
				"MenuNumber": "6",
				"Products": [{
					"ProductId": 440004,
					"Price": 25.5,
					"Name": "Fisch Rotes Thai Curry (leicht scharf)",
					"Description": "Gem\u00fcse, Kokosmilch, rote Currypaste, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "6",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Fisch Paneng Curry (mild scharf)",
				"MenuNumber": "7",
				"Products": [{
					"ProductId": 440010,
					"Price": 25.5,
					"Name": "Fisch Paneng Curry (mild scharf)",
					"Description": "Thai Kokos-Curry mit Zitronenbl\u00e4ttern, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "7",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Fisch Gelbes Curry (nicht scharf)",
				"MenuNumber": "8",
				"Products": [{
					"ProductId": 440016,
					"Price": 25.5,
					"Name": "Fisch Gelbes Curry (nicht scharf)",
					"Description": "Gem\u00fcse, Kokosmilch, gelbe Currypaste, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "8",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Fisch Pad Grapao (sehr scharf)",
				"MenuNumber": "9",
				"Products": [{
					"ProductId": 440022,
					"Price": 25.5,
					"Name": "Fisch Pad Grapao (sehr scharf)",
					"Description": "Gebratene Zutaten mit Thai Basilikum, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "9",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}]
		}, {
			"Id": 12910,
			"Name": "Crevettengerichte",
			"Description": "",
			"Offer": "NoOffer",
			"MenuItems": [{
				"Name": "Crevetten S\u00fcss Sauer mit Teig",
				"MenuNumber": "1",
				"Products": [{
					"ProductId": 106775,
					"Price": 29,
					"Name": "Crevetten S\u00fcss Sauer mit Teig",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "1",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 397284,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73375
					}, {
						"Id": 397285,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73375
					}, {
						"Id": 397286,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73375
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Crevetten Szechuan Art",
				"MenuNumber": "2",
				"Products": [{
					"ProductId": 106776,
					"Price": 29,
					"Name": "Crevetten Szechuan Art",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "2",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 397287,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73376
					}, {
						"Id": 397288,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73376
					}, {
						"Id": 397289,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73376
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Crevetten KungBao",
				"MenuNumber": "3",
				"Products": [{
					"ProductId": 106777,
					"Price": 30,
					"Name": "Crevetten KungBao",
					"Description": "Crevetten mit Cashewn\u00fcssen",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "3",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 397290,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73377
					}, {
						"Id": 397291,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73377
					}, {
						"Id": 397292,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73377
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Crevetten an Sataysauce",
				"MenuNumber": "4",
				"Products": [{
					"ProductId": 106778,
					"Price": 29,
					"Name": "Crevetten an Sataysauce",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "4",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 397293,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73378
					}, {
						"Id": 397294,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73378
					}, {
						"Id": 397295,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73378
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Crevetten mit verschiedenem Saisongem\u00fcse",
				"MenuNumber": "5",
				"Products": [{
					"ProductId": 106779,
					"Price": 29,
					"Name": "Crevetten mit verschiedenem Saisongem\u00fcse",
					"Description": "Crevetten Gem\u00fcse, mit Soja-Sauce gebraten",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "5",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1552705,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73379
					}, {
						"Id": 1552706,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73379
					}, {
						"Id": 1552707,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73379
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Crevetten mit chinesischen Pilzen und Bambus",
				"MenuNumber": "6",
				"Products": [{
					"ProductId": 106780,
					"Price": 30,
					"Name": "Crevetten mit chinesischen Pilzen und Bambus",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "6",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 397299,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73380
					}, {
						"Id": 397300,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73380
					}, {
						"Id": 397301,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73380
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Crevetten Gr\u00fcnes Curry (scharf)",
				"MenuNumber": "7",
				"Products": [{
					"ProductId": 439999,
					"Price": 28.5,
					"Name": "Crevetten Gr\u00fcnes Curry (scharf)",
					"Description": "Gem\u00fcse, Kokosmilch, gr\u00fcne Currypaste, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "7",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Crevetten Rotes Thai Curry (leicht scharf)",
				"MenuNumber": "8",
				"Products": [{
					"ProductId": 440005,
					"Price": 28.5,
					"Name": "Crevetten Rotes Thai Curry (leicht scharf)",
					"Description": "Gem\u00fcse, Kokosmilch, rote Currypaste, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "8",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Crevetten Paneng Curry (mild scharf)",
				"MenuNumber": "9",
				"Products": [{
					"ProductId": 440011,
					"Price": 28.5,
					"Name": "Crevetten Paneng Curry (mild scharf)",
					"Description": "Thai Kokos-Curry mit Zitronenbl\u00e4ttern, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "9",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Crevetten Gelbes Curry (nicht scharf)",
				"MenuNumber": "10",
				"Products": [{
					"ProductId": 440017,
					"Price": 28.5,
					"Name": "Crevetten Gelbes Curry (nicht scharf)",
					"Description": "Gem\u00fcse, Kokosmilch, gelbe Currypaste, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "10",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Crevetten Pad Grapao (sehr scharf)",
				"MenuNumber": "11",
				"Products": [{
					"ProductId": 440023,
					"Price": 28.5,
					"Name": "Crevetten Pad Grapao (sehr scharf)",
					"Description": "Gebratene Zutaten mit Thai Basilikum, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "11",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 2039936,
						"Name": "Pad Grapao (sehr scharf)",
						"Price": 0,
						"GroupId": 429544
					}],
					"MealParts": [],
					"Tags": []
				}]
			}]
		}, {
			"Id": 12911,
			"Name": "Entengerichte",
			"Description": "",
			"Offer": "NoOffer",
			"MenuItems": [{
				"Name": "Knusprige halbe Ente PakLokHaus",
				"MenuNumber": "2",
				"Products": [{
					"ProductId": 106781,
					"Price": 35.5,
					"Name": "Knusprige halbe Ente PakLokHaus",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "2",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 397302,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73381
					}, {
						"Id": 397303,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73381
					}, {
						"Id": 397304,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73381
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Ente an Orangensauce Pak Lok Haus",
				"MenuNumber": "3",
				"Products": [{
					"ProductId": 106782,
					"Price": 35.5,
					"Name": "Ente an Orangensauce Pak Lok Haus",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "3",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 397305,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73382
					}, {
						"Id": 397306,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73382
					}, {
						"Id": 397307,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73382
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Ente Szechuan Art",
				"MenuNumber": "4",
				"Products": [{
					"ProductId": 160026,
					"Price": 28.5,
					"Name": "Ente Szechuan Art",
					"Description": "gebackene Ente in Scheiben geschnitten mit Gem\u00fcse an scharfer Sauce",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "4",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 397396,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 121810
					}, {
						"Id": 397397,
						"Name": "Gebratener Reis",
						"Price": 3,
						"GroupId": 121810
					}, {
						"Id": 397398,
						"Name": "Gebratene Nudeln",
						"Price": 3,
						"GroupId": 121810
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Knusprige Ente S\u00fcss-Sauer",
				"MenuNumber": "1",
				"Products": [{
					"ProductId": 389958,
					"Price": 25,
					"Name": "Knusprige Ente S\u00fcss-Sauer",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "1",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1713097,
						"Name": "Jasminreis",
						"Price": 0,
						"GroupId": 373049
					}, {
						"Id": 1713098,
						"Name": "gebratener Reis",
						"Price": 3,
						"GroupId": 373049
					}, {
						"Id": 1713099,
						"Name": "gebratene Nudeln",
						"Price": 3,
						"GroupId": 373049
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Knusprige Ente Gr\u00fcnes Curry (scharf)",
				"MenuNumber": "5",
				"Products": [{
					"ProductId": 440000,
					"Price": 28.5,
					"Name": "Knusprige Ente Gr\u00fcnes Curry (scharf)",
					"Description": "Gem\u00fcse, Kokosmilch, gr\u00fcne Currypaste, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "5",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Knusprige Ente Rotes Thai Curry (leicht scharf)",
				"MenuNumber": "6",
				"Products": [{
					"ProductId": 440007,
					"Price": 28.5,
					"Name": "Knusprige Ente Rotes Thai Curry (leicht scharf)",
					"Description": "Gem\u00fcse, Kokosmilch, rote Currypaste, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "6",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Knusprige Ente Paneng Curry (mild scharf)",
				"MenuNumber": "7",
				"Products": [{
					"ProductId": 440013,
					"Price": 28.5,
					"Name": "Knusprige Ente Paneng Curry (mild scharf)",
					"Description": "Thai Kokos-Curry mit Zitronenbl\u00e4ttern, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "7",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Knusprige Ente Gelbes Curry (nicht scharf)",
				"MenuNumber": "8",
				"Products": [{
					"ProductId": 440019,
					"Price": 28.5,
					"Name": "Knusprige Ente Gelbes Curry (nicht scharf)",
					"Description": "Gebratene Zutaten mit Thai Basilikum, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "8",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}]
		}, {
			"Id": 12912,
			"Name": "Rindfleischgerichte",
			"Description": "",
			"Offer": "NoOffer",
			"MenuItems": [{
				"Name": "Rindfleisch Ananas S\u00fcss Sauer (ohne Teig)",
				"MenuNumber": "1",
				"Products": [{
					"ProductId": 106783,
					"Price": 25,
					"Name": "Rindfleisch Ananas S\u00fcss Sauer (ohne Teig)",
					"Description": "Rindfleisch mit Ananas, Karotten, Peperone Sweet-Sauer Sauce",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "1",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1552735,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73383
					}, {
						"Id": 1552736,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73383
					}, {
						"Id": 1552737,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73383
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Rindfleisch Szechuan (scharf)",
				"MenuNumber": "2",
				"Products": [{
					"ProductId": 106784,
					"Price": 25,
					"Name": "Rindfleisch Szechuan (scharf)",
					"Description": "Rindfleisch und Saisongem\u00fcse nach Szechuan Art",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "2",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 397311,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73384
					}, {
						"Id": 397312,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73384
					}, {
						"Id": 397313,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73384
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Rindfleisch KungBao (mild und sauer)",
				"MenuNumber": "3",
				"Products": [{
					"ProductId": 106785,
					"Price": 26,
					"Name": "Rindfleisch KungBao (mild und sauer)",
					"Description": "Rindfleisch, Kerneln\u00fcsse und Saisongem\u00fcse an einer dunklen Sauce",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "3",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 397314,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73385
					}, {
						"Id": 397315,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73385
					}, {
						"Id": 397316,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73385
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Rindfleisch an Sataysauce",
				"MenuNumber": "4",
				"Products": [{
					"ProductId": 106786,
					"Price": 25,
					"Name": "Rindfleisch an Sataysauce",
					"Description": "Rindfleisch und Saisongem\u00fcse nach PakLok Art an einer Sataysauce",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "4",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1653003,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73386
					}, {
						"Id": 1653004,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73386
					}, {
						"Id": 1653005,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73386
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Rindfleisch mit verschiedenem Saisongem\u00fcse",
				"MenuNumber": "5",
				"Products": [{
					"ProductId": 106787,
					"Price": 25,
					"Name": "Rindfleisch mit verschiedenem Saisongem\u00fcse",
					"Description": "Rindfleisch Gem\u00fcse, mit Soja-Sauce gebraten",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "5",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1552708,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73387
					}, {
						"Id": 1552709,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73387
					}, {
						"Id": 1552710,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73387
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Rindfleisch mit chinesischen Pilzen und Bambussprossen",
				"MenuNumber": "6",
				"Products": [{
					"ProductId": 106788,
					"Price": 26,
					"Name": "Rindfleisch mit chinesischen Pilzen und Bambussprossen",
					"Description": "Saisongem\u00fcse und dazu eine Soja-Sauce",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "6",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 397323,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73388
					}, {
						"Id": 397324,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73388
					}, {
						"Id": 397325,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73388
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Rindfleisch mit Zwiebeln",
				"MenuNumber": "7",
				"Products": [{
					"ProductId": 106789,
					"Price": 25,
					"Name": "Rindfleisch mit Zwiebeln",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "7",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 397326,
						"Name": "Jasmin Reis",
						"Price": 0,
						"GroupId": 73389
					}, {
						"Id": 397327,
						"Name": "Gebratener Reis",
						"Price": 2.5,
						"GroupId": 73389
					}, {
						"Id": 397328,
						"Name": "Gebratene Nudeln",
						"Price": 2.5,
						"GroupId": 73389
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Rindfleisch Gr\u00fcnes Curry (scharf)",
				"MenuNumber": "8",
				"Products": [{
					"ProductId": 439997,
					"Price": 25.5,
					"Name": "Rindfleisch Gr\u00fcnes Curry (scharf)",
					"Description": "Gem\u00fcse, Kokosmilch, gr\u00fcne Currypaste, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "8",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Rindfleisch Rotes Thai Curry (leicht scharf)",
				"MenuNumber": "9",
				"Products": [{
					"ProductId": 440003,
					"Price": 25.5,
					"Name": "Rindfleisch Rotes Thai Curry (leicht scharf)",
					"Description": "Gem\u00fcse, Kokosmilch, rote Currypaste, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "9",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Rindfleisch Paneng Curry (mild scharf)",
				"MenuNumber": "10",
				"Products": [{
					"ProductId": 440009,
					"Price": 25.5,
					"Name": "Rindfleisch Paneng Curry (mild scharf)",
					"Description": "Thai Kokos-Curry mit Zitronenbl\u00e4ttern, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "10",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Rindfleisch Gelbes Curry (nicht scharf)",
				"MenuNumber": "11",
				"Products": [{
					"ProductId": 440015,
					"Price": 25.5,
					"Name": "Rindfleisch Gelbes Curry (nicht scharf)",
					"Description": "Gebratene Zutaten mit Thai Basilikum, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "11",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Rindfleisch Pad Grapao (sehr scharf)",
				"MenuNumber": "12",
				"Products": [{
					"ProductId": 440021,
					"Price": 25.5,
					"Name": "Rindfleisch Pad Grapao (sehr scharf)",
					"Description": "Gebratene Zutaten mit Thai Basilikum, serviert mit Jasmin Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "12",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}]
		}, {
			"Id": 12897,
			"Name": "Gem\u00fcse- und Nudelgerichte",
			"Description": "",
			"Offer": "NoOffer",
			"MenuItems": [{
				"Name": "Gem\u00fcse Chop Suey",
				"MenuNumber": "3",
				"Products": [{
					"ProductId": 106689,
					"Price": 15.5,
					"Name": "Gem\u00fcse Chop Suey",
					"Description": "Serviert mit Saisongem\u00fcse und ohne Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "3",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Gebratene chinesische Pilze und Bambussprossen",
				"MenuNumber": "4",
				"Products": [{
					"ProductId": 106690,
					"Price": 16.5,
					"Name": "Gebratene chinesische Pilze und Bambussprossen",
					"Description": "Ohne Reis serviert",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "4",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Gebratenes Pak Choi Gem\u00fcse",
				"MenuNumber": "5",
				"Products": [{
					"ProductId": 106691,
					"Price": 16.5,
					"Name": "Gebratenes Pak Choi Gem\u00fcse",
					"Description": "Serviert mit Saisongem\u00fcse einer Kikoma-Soja Sauce und ohne Reis",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "5",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Gebratene Nudeln",
				"MenuNumber": "7",
				"Products": [{
					"ProductId": 106692,
					"Price": 13.5,
					"Name": "Gebratene Nudeln",
					"Description": "Saisongem\u00fcse, Ei und bitte w\u00e4hlen Sie eine Zutat:",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "7",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1652958,
						"Name": "Gem\u00fcse",
						"Price": 0,
						"GroupId": 73345
					}, {
						"Id": 1652959,
						"Name": "Poulet und Gem\u00fcse",
						"Price": 8,
						"GroupId": 73345
					}, {
						"Id": 1652960,
						"Name": "Rindfleisch und Gem\u00fcse",
						"Price": 10,
						"GroupId": 73345
					}, {
						"Id": 1652961,
						"Name": "Crevetten und Gem\u00fcse",
						"Price": 13,
						"GroupId": 73345
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Gebratene Reis-Nudeln",
				"MenuNumber": "6",
				"Products": [{
					"ProductId": 106693,
					"Price": 26.5,
					"Name": "Gebratene Reis-Nudeln",
					"Description": "Serviert mit Crevetten nach Pak Lok Haus Art",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "6",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Gebratener Reis ",
				"MenuNumber": "8",
				"Products": [{
					"ProductId": 106694,
					"Price": 13.5,
					"Name": "Gebratener Reis ",
					"Description": "Bitte w\u00e4hlen Sie eine Zutat:",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "8",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1652962,
						"Name": "Gem\u00fcse",
						"Price": 0,
						"GroupId": 73346
					}, {
						"Id": 1652963,
						"Name": "Poulet und Gem\u00fcse",
						"Price": 8,
						"GroupId": 73346
					}, {
						"Id": 1652964,
						"Name": "Rindfleisch und Gem\u00fcse",
						"Price": 10,
						"GroupId": 73346
					}, {
						"Id": 1652965,
						"Name": "Crevetten und Gem\u00fcse",
						"Price": 13,
						"GroupId": 73346
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Udonnudeln",
				"MenuNumber": "9",
				"Products": [{
					"ProductId": 106695,
					"Price": 21.5,
					"Name": "Udonnudeln",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "9",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 1652966,
						"Name": "Tofu",
						"Price": 0,
						"GroupId": 73347
					}, {
						"Id": 1652967,
						"Name": "Poulet",
						"Price": 0,
						"GroupId": 73347
					}, {
						"Id": 1652968,
						"Name": "Rindfleisch",
						"Price": 2,
						"GroupId": 73347
					}, {
						"Id": 1652969,
						"Name": "Crevetten",
						"Price": 5,
						"GroupId": 73347
					}, {
						"Id": 1652970,
						"Name": "Ente",
						"Price": 3,
						"GroupId": 73347
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Hit-Menu Ya-Chao Fan",
				"MenuNumber": "1",
				"Products": [{
					"ProductId": 403197,
					"Price": 23,
					"Name": "Hit-Menu Ya-Chao Fan",
					"Description": "Gebratener Reis mit Entenfleisch und Gem\u00fcse",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "1",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Hit-Menu Ya-Chao Mian",
				"MenuNumber": "2",
				"Products": [{
					"ProductId": 403198,
					"Price": 23,
					"Name": "Hit-Menu Ya-Chao Mian",
					"Description": "Gebratene Nudeln mit Entenfleisch und Gem\u00fcse",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "2",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}]
		}, {
			"Id": 12914,
			"Name": "Beilagen",
			"Description": "",
			"Offer": "NoOffer",
			"MenuItems": [{
				"Name": "Jasmin Reis",
				"MenuNumber": "1",
				"Products": [{
					"ProductId": 106795,
					"Price": 2.5,
					"Name": "Jasmin Reis",
					"Description": "1 Schale",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "1",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Gebratener Reis",
				"MenuNumber": "2",
				"Products": [{
					"ProductId": 106796,
					"Price": 5,
					"Name": "Gebratener Reis",
					"Description": "1 Schale",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "2",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Gebratene Nudeln",
				"MenuNumber": "3",
				"Products": [{
					"ProductId": 106797,
					"Price": 5,
					"Name": "Gebratene Nudeln",
					"Description": "1 Schale",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "3",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [],
					"MealParts": [],
					"Tags": []
				}]
			}]
		}, {
			"Id": 12898,
			"Name": "Softdrinks",
			"Description": "",
			"Offer": "NoOffer",
			"MenuItems": [{
				"Name": "Valser",
				"MenuNumber": "1",
				"Products": [{
					"ProductId": 106696,
					"Price": 4.5,
					"Name": "Valser",
					"Description": "Mineralwasser mit Kohlens\u00e4ure",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "1",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 208535,
						"Name": "0.5l",
						"Price": 0,
						"GroupId": 73348
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Evian",
				"MenuNumber": "2",
				"Products": [{
					"ProductId": 106697,
					"Price": 4.5,
					"Name": "Evian",
					"Description": "Mineralwasser ohne Kohlens\u00e4ure",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "2",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 208536,
						"Name": "0.5 l",
						"Price": 0,
						"GroupId": 73349
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Ice Tea Lemon",
				"MenuNumber": "3",
				"Products": [{
					"ProductId": 106698,
					"Price": 4.5,
					"Name": "Ice Tea Lemon",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "3",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 208537,
						"Name": "0.5 l",
						"Price": 0,
						"GroupId": 73350
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Coca Cola",
				"MenuNumber": "4",
				"Products": [{
					"ProductId": 106699,
					"Price": 4.5,
					"Name": "Coca Cola",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "4",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 208538,
						"Name": "0.5 l",
						"Price": 0,
						"GroupId": 73351
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Coca Cola Zero",
				"MenuNumber": "5",
				"Products": [{
					"ProductId": 106700,
					"Price": 4.5,
					"Name": "Coca Cola Zero",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "5",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 208539,
						"Name": "0.5 l",
						"Price": 0,
						"GroupId": 73352
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Oishi Tee, Gr\u00fcn Tee",
				"MenuNumber": "6",
				"Products": [{
					"ProductId": 106701,
					"Price": 5.5,
					"Name": "Oishi Tee, Gr\u00fcn Tee",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "6",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 208540,
						"Name": "0.5 l",
						"Price": 0,
						"GroupId": 73353
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Oishi Tee Honig",
				"MenuNumber": "7",
				"Products": [{
					"ProductId": 106702,
					"Price": 5.5,
					"Name": "Oishi Tee Honig",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "7",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 208541,
						"Name": "0.5 l",
						"Price": 0,
						"GroupId": 73354
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Kokosnussdrink",
				"MenuNumber": "8",
				"Products": [{
					"ProductId": 106703,
					"Price": 4.5,
					"Name": "Kokosnussdrink",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "8",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 208542,
						"Name": "0.35 l",
						"Price": 0,
						"GroupId": 73355
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Mangodrink",
				"MenuNumber": "9",
				"Products": [{
					"ProductId": 106704,
					"Price": 4.5,
					"Name": "Mangodrink",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "9",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 208543,
						"Name": "0.35 l",
						"Price": 0,
						"GroupId": 73356
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Red Bull",
				"MenuNumber": "10",
				"Products": [{
					"ProductId": 106705,
					"Price": 5,
					"Name": "Red Bull",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "10",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 208544,
						"Name": "0.25 l",
						"Price": 0,
						"GroupId": 73357
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Lychee Drink",
				"MenuNumber": "11",
				"Products": [{
					"ProductId": 203098,
					"Price": 4.5,
					"Name": "Lychee Drink",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "11",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 685410,
						"Name": "0.35 l",
						"Price": 0,
						"GroupId": 173578
					}],
					"MealParts": [],
					"Tags": []
				}]
			}]
		}, {
			"Id": 12915,
			"Name": "Bier",
			"Description": "",
			"Offer": "NoOffer",
			"MenuItems": [{
				"Name": "Tsingtao",
				"MenuNumber": "1",
				"Products": [{
					"ProductId": 106798,
					"Price": 5.5,
					"Name": "Tsingtao",
					"Description": "China",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "1",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 208669,
						"Name": "0.35l",
						"Price": 0,
						"GroupId": 73395
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Singha",
				"MenuNumber": "2",
				"Products": [{
					"ProductId": 106799,
					"Price": 5.5,
					"Name": "Singha",
					"Description": "Thailand",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "2",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 208670,
						"Name": "0.35l",
						"Price": 0,
						"GroupId": 73396
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Asahi",
				"MenuNumber": "3",
				"Products": [{
					"ProductId": 106800,
					"Price": 6,
					"Name": "Asahi",
					"Description": "Japan",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "3",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 208671,
						"Name": "0.35l",
						"Price": 0,
						"GroupId": 73397
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Haldenkrone Premium",
				"MenuNumber": "4",
				"Products": [{
					"ProductId": 106801,
					"Price": 4.8,
					"Name": "Haldenkrone Premium",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "4",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 208672,
						"Name": "0.35l",
						"Price": 0,
						"GroupId": 73398
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Clausthaler Alkoholfrei",
				"MenuNumber": "5",
				"Products": [{
					"ProductId": 106802,
					"Price": 4.8,
					"Name": "Clausthaler Alkoholfrei",
					"Description": "",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "5",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 208673,
						"Name": "0.35l",
						"Price": 0,
						"GroupId": 73399
					}],
					"MealParts": [],
					"Tags": []
				}]
			}, {
				"Name": "Japanische Sake",
				"MenuNumber": "6",
				"Products": [{
					"ProductId": 160014,
					"Price": 15.5,
					"Name": "Japanische Sake",
					"Description": "Reiswein",
					"Synonym": "",
					"ProductTypeId": 0,
					"RequireOtherProducts": false,
					"MenuNumber": "6",
					"Offer": "NoOffer",
					"OptionalAccessories": [],
					"RequiredAccessories": [{
						"Id": 685409,
						"Name": "0.3 l",
						"Price": 0,
						"GroupId": 121798
					}],
					"MealParts": [],
					"Tags": []
				}]
			}]
		}]
	},
	"PaymentMethods": [{
		"MethodId": 1,
		"MethodName": "Cash"
	}, {
		"MethodId": 2,
		"MethodName": "VISA"
	}, {
		"MethodId": 3,
		"MethodName": "MasterCard"
	}, {
		"MethodId": 4,
		"MethodName": "PostFinance Card"
	}, {
		"MethodId": 5,
		"MethodName": "PostFinance e-finance"
	}, {
		"MethodId": 6,
		"MethodName": "American Express"
	}, {
		"MethodId": 7,
		"MethodName": "Swisscom Easypay"
	}, {
		"MethodId": 8,
		"MethodName": "PayPal"
	}],
	"ErrorResponse": null
}

export default menuAndPayment;
