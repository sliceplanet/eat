const reviews = {
  "RestaurantId": 1515,
  "PageNumber": 0,
  "PageSize": 30,
  "Reviews": [{
    "UserId": 1976716,
    "RestaurantId": 1515,
    "CustomerCity": "Z\u00fcrich",
    "CustomerComments": "So weiter machen",
    "CustomerName": "Sandra B.",
    "RateDate": "2018-08-12T22:57:19Z",
    "RatingAverage": 4.67,
    "RatingAverageStars": 4.67
  }, {
    "UserId": 4944607,
    "RestaurantId": 1515,
    "CustomerCity": "ZURICH",
    "CustomerComments": "",
    "CustomerName": "Jose Francisco P.",
    "RateDate": "2018-08-12T18:48:44Z",
    "RatingAverage": 4.33,
    "RatingAverageStars": 4.33
  }, {
    "UserId": 4926453,
    "RestaurantId": 1515,
    "CustomerCity": "D\u00fcbendorf",
    "CustomerComments": "",
    "CustomerName": "Diogo A.",
    "RateDate": "2018-08-11T23:17:54Z",
    "RatingAverage": 5,
    "RatingAverageStars": 5
  }, {
    "UserId": 4682768,
    "RestaurantId": 1515,
    "CustomerCity": "Z\u00fcrich",
    "CustomerComments": "",
    "CustomerName": "Andrea P.",
    "RateDate": "2018-08-12T07:48:36Z",
    "RatingAverage": 5,
    "RatingAverageStars": 5
  }, {
    "UserId": 4754722,
    "RestaurantId": 1515,
    "CustomerCity": "Wallisellen",
    "CustomerComments": "",
    "CustomerName": "Vanessa C.",
    "RateDate": "2018-08-11T21:43:59Z",
    "RatingAverage": 5,
    "RatingAverageStars": 5
  }, {
    "UserId": 2533147,
    "RestaurantId": 1515,
    "CustomerCity": "R\u00fcmlang",
    "CustomerComments": "",
    "CustomerName": "Cinzia Z.",
    "RateDate": "2018-08-11T06:07:48Z",
    "RatingAverage": 3,
    "RatingAverageStars": 3
  }, {
    "UserId": 3991404,
    "RestaurantId": 1515,
    "CustomerCity": "Wallisellen",
    "CustomerComments": "",
    "CustomerName": "Ayan S.",
    "RateDate": "2018-08-10T13:01:27Z",
    "RatingAverage": 5,
    "RatingAverageStars": 5
  }, {
    "UserId": 4686589,
    "RestaurantId": 1515,
    "CustomerCity": "D\u00fcbendorf",
    "CustomerComments": "Das Essen war nicht lecker, w\u00fcrde es nicht empfehlen. Lieferzeit und Freundlichkeit sehr gut.",
    "CustomerName": "Ernestina T.",
    "RateDate": "2018-08-10T07:50:34Z",
    "RatingAverage": 3,
    "RatingAverageStars": 3
  }, {
    "UserId": 4529831,
    "RestaurantId": 1515,
    "CustomerCity": "Wallisellen",
    "CustomerComments": "",
    "CustomerName": "Elizabeta P.",
    "RateDate": "2018-08-10T00:45:24Z",
    "RatingAverage": 4,
    "RatingAverageStars": 4
  }, {
    "UserId": 4695124,
    "RestaurantId": 1515,
    "CustomerCity": "Wallisellen",
    "CustomerComments": "",
    "CustomerName": "Juan M.",
    "RateDate": "2018-08-10T06:32:59Z",
    "RatingAverage": 5,
    "RatingAverageStars": 5
  }, {
    "UserId": 4490806,
    "RestaurantId": 1515,
    "CustomerCity": "Wallisellen",
    "CustomerComments": "Danke f\u00fcrs das zus\u00e4tzliche Getr\u00e4nk",
    "CustomerName": "Heike K.",
    "RateDate": "2018-08-09T13:52:34Z",
    "RatingAverage": 4.33,
    "RatingAverageStars": 4.33
  }, {
    "UserId": 4753814,
    "RestaurantId": 1515,
    "CustomerCity": "Wallisellen",
    "CustomerComments": "",
    "CustomerName": "Pascal B.",
    "RateDate": "2018-08-08T23:07:47Z",
    "RatingAverage": 4.67,
    "RatingAverageStars": 4.67
  }, {
    "UserId": 4487163,
    "RestaurantId": 1515,
    "CustomerCity": "D\u00fcbendorf",
    "CustomerComments": "",
    "CustomerName": "Adrian E.",
    "RateDate": "2018-08-10T17:04:42Z",
    "RatingAverage": 4.33,
    "RatingAverageStars": 4.33
  }, {
    "UserId": 4969065,
    "RestaurantId": 1515,
    "CustomerCity": "Wallisellen",
    "CustomerComments": "",
    "CustomerName": "Silena V.",
    "RateDate": "2018-08-07T20:58:24Z",
    "RatingAverage": 5,
    "RatingAverageStars": 5
  }, {
    "UserId": 1149683,
    "RestaurantId": 1515,
    "CustomerCity": "Wallisellen",
    "CustomerComments": "Kurier war sehr freundlich und Essen kam auf die Minute genau, also sehr p\u00fcnktlich!\r\n\r\nLeider waren die Fr\u00fchlingsrollen kalt, deswegen nur 4\/5 Sterne.",
    "CustomerName": "Stefan W.",
    "RateDate": "2018-08-07T14:18:46Z",
    "RatingAverage": 4.67,
    "RatingAverageStars": 4.67
  }, {
    "UserId": 4316761,
    "RestaurantId": 1515,
    "CustomerCity": "Glattpark (Opfikon)",
    "CustomerComments": "The food is AS ALWAYS very tasty, the staff at the restaurant (on the phone) was very friendly, the food arrived a bit earlier than announced. Due that the driver was occupied on the phone (it sound privately) he missed to read the note which floor I'm living and didn't even bother to greet when he finally was here.",
    "CustomerName": "Berry B.",
    "RateDate": "2018-08-06T10:23:13Z",
    "RatingAverage": 3.67,
    "RatingAverageStars": 3.67
  }, {
    "UserId": 4067127,
    "RestaurantId": 1515,
    "CustomerCity": "Wallisellen",
    "CustomerComments": "Sehr gutes Essen!",
    "CustomerName": "Antun B.",
    "RateDate": "2018-08-05T21:27:34Z",
    "RatingAverage": 5,
    "RatingAverageStars": 5
  }, {
    "UserId": 3924378,
    "RestaurantId": 1515,
    "CustomerCity": "Wallisellen",
    "CustomerComments": "",
    "CustomerName": "Lucas S.",
    "RateDate": "2018-08-05T06:07:54Z",
    "RatingAverage": 5,
    "RatingAverageStars": 5
  }, {
    "UserId": 4241526,
    "RestaurantId": 1515,
    "CustomerCity": "Glattpark",
    "CustomerComments": "Sensationelles, frisches und schmackhaftes Essen, heiss geliefert von einem freundlichen Kurier. Alle W\u00fcnsche erf\u00fcllt.",
    "CustomerName": "Stephan M.",
    "RateDate": "2018-08-03T23:00:32Z",
    "RatingAverage": 4.67,
    "RatingAverageStars": 4.67
  }, {
    "UserId": 4821776,
    "RestaurantId": 1515,
    "CustomerCity": "Dietlikon",
    "CustomerComments": "",
    "CustomerName": "Michael E.",
    "RateDate": "2018-08-04T09:47:17Z",
    "RatingAverage": 5,
    "RatingAverageStars": 5
  }, {
    "UserId": 3527203,
    "RestaurantId": 1515,
    "CustomerCity": "D\u00fcbendorf",
    "CustomerComments": "Sehr gutes Sushi!!!",
    "CustomerName": "Birgit J.",
    "RateDate": "2018-08-03T13:54:28Z",
    "RatingAverage": 4.67,
    "RatingAverageStars": 4.67
  }, {
    "UserId": 3327705,
    "RestaurantId": 1515,
    "CustomerCity": "Z\u00fcrich",
    "CustomerComments": "",
    "CustomerName": "Dominik S.",
    "RateDate": "2018-08-02T22:41:52Z",
    "RatingAverage": 5,
    "RatingAverageStars": 5
  }, {
    "UserId": 4027310,
    "RestaurantId": 1515,
    "CustomerCity": "Wallisellen",
    "CustomerComments": "",
    "CustomerName": "Corina H.",
    "RateDate": "2018-08-05T14:48:32Z",
    "RatingAverage": 4.33,
    "RatingAverageStars": 4.33
  }, {
    "UserId": 4422568,
    "RestaurantId": 1515,
    "CustomerCity": "Z\u00fcrich",
    "CustomerComments": "",
    "CustomerName": "Val\u00e9rie J.",
    "RateDate": "2018-08-02T01:30:40Z",
    "RatingAverage": 4,
    "RatingAverageStars": 4
  }, {
    "UserId": 4451535,
    "RestaurantId": 1515,
    "CustomerCity": "Z\u00fcrich",
    "CustomerComments": "",
    "CustomerName": "juliet d.",
    "RateDate": "2018-08-02T07:30:03Z",
    "RatingAverage": 5,
    "RatingAverageStars": 5
  }, {
    "UserId": 4845253,
    "RestaurantId": 1515,
    "CustomerCity": "Z\u00fcrich",
    "CustomerComments": "",
    "CustomerName": "Sabrina M.",
    "RateDate": "2018-08-01T22:58:54Z",
    "RatingAverage": 4,
    "RatingAverageStars": 4
  }, {
    "UserId": 4964614,
    "RestaurantId": 1515,
    "CustomerCity": "Wallisellen",
    "CustomerComments": "Alles gut. ",
    "CustomerName": "Fausto M.",
    "RateDate": "2018-08-02T06:48:36Z",
    "RatingAverage": 5,
    "RatingAverageStars": 5
  }, {
    "UserId": 4912765,
    "RestaurantId": 1515,
    "CustomerCity": "Wallisellen",
    "CustomerComments": "",
    "CustomerName": "Robert S.",
    "RateDate": "2018-08-02T11:22:32Z",
    "RatingAverage": 5,
    "RatingAverageStars": 5
  }, {
    "UserId": 4298010,
    "RestaurantId": 1515,
    "CustomerCity": "N\u00fcrensdorf",
    "CustomerComments": "",
    "CustomerName": "Alain G.",
    "RateDate": "2018-08-01T18:42:25Z",
    "RatingAverage": 4.33,
    "RatingAverageStars": 4.33
  }, {
    "UserId": 2877556,
    "RestaurantId": 1515,
    "CustomerCity": "Wallisellen",
    "CustomerComments": "Ich habe gebratene Nuddeln bestellt aber bekommen habe ich gebratenen Reis",
    "CustomerName": "Debora D.",
    "RateDate": "2018-08-01T17:07:40Z",
    "RatingAverage": 5,
    "RatingAverageStars": 5
  }]
}



export default reviews;
