const ratingTypes = [
    {
    "className": 'fiveStars',
    "percentage": '70',
    "starAmount": 5
  
},
    {
        "className": 'fourStars',
        "percentage": '19',
        "starAmount": 4

    },
    {
        "className": 'threeStars',
        "percentage": '6',
        "starAmount": 3

    },
    {
        "className": 'twoStars',
        "percentage": '3',
        "starAmount": 2

    }, {
        "className": 'oneStar',
        "percentage": '1',
        "starAmount": 1

    }
]
export default ratingTypes;