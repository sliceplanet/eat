const schedule =  {
    "Id": 1515,
        "Description": "Bei Barbezahlung werden Lunch-Checks akzeptiert.\r\n",
            "LocalOpeningTimes": [{
                "Key": "Monday",
                "Times": [{
                    "Open": "",
                    "Closed": ""
                }, {
                    "Open": "",
                    "Closed": ""
                }]
            }, {
                "Key": "Tuesday",
                "Times": [{
                    "Open": "10:00",
                    "Closed": "14:00"
                }, {
                    "Open": "17:00",
                    "Closed": "22:00"
                }]
            }, {
                "Key": "Wednesday",
                "Times": [{
                    "Open": "10:00",
                    "Closed": "14:00"
                }, {
                    "Open": "17:00",
                    "Closed": "22:00"
                }]
            }, {
                "Key": "Thursday",
                "Times": [{
                    "Open": "10:00",
                    "Closed": "14:00"
                }, {
                    "Open": "17:00",
                    "Closed": "22:00"
                }]
            }, {
                "Key": "Friday",
                "Times": [{
                    "Open": "10:00",
                    "Closed": "14:00"
                }, {
                    "Open": "17:00",
                    "Closed": "22:00"
                }]
            }, {
                "Key": "Saturday",
                "Times": [{
                    "Open": "",
                    "Closed": ""
                }, {
                    "Open": "15:30",
                    "Closed": "22:00"
                }]
            }, {
                "Key": "Sunday",
                "Times": [{
                    "Open": "11:00",
                    "Closed": "15:00"
                }, {
                    "Open": "15:00",
                    "Closed": "22:00"
                }]
            }]
}
export default schedule;
