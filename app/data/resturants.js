const resturants = {
    "Id": 1515,
    "Name": "Pak Lok Haus",
    "Address": "Bubentalstrasse 7",
    "Postcode": "8304",
    "City": "Wallisellen",
    "CuisineTypes": [{
        "Id": 2,
        "Name": "chinese"
    }, {
        "Id": 10,
        "Name": "thai"
    }, {
        "Id": 11,
        "Name": "asian"
    }, {
        "Id": 25,
        "Name": "curry"
    }, {
        "Id": 22,
        "Name": "sushi"
    }],
    "Url": "https:\/\/www.eat.ch\/restaurant\/pak-lok-haus\/menu",
    "IsSponsored": false,
    "NewnessDate": "2014-04-12T19:30:37Z",
    "IsNew": false,
    "IsTemporarilyOffline": false,
    "ReasonWhyTemporarilyOffline": false,
    "UniqueName": "pak-lok-haus",
    "IsHalal": false,
    "IsOpenNowForDelivery": false,
    "IsOpenNowForCollection": false,
    "RatingStars": 4.59,
    "DeliveryCost": 0,
    "IsCollection": false,
    "IsDelivery": true,
    "Deals": [],
    "Logo": [{
        "StandardResolutionURL": "https:\/\/static.eat.ch\/mod_logos\/pak-lok-haus.png"
    }],
    "NumberOfRatings": 3098,
    "IsOpenNow": false,
    "DeliveryOpeningTime": "",
    "DefaultDisplayRank": 48
}
export default resturants;
