const topRestaurant = {
	"ShortResultText": "8057",
	"Restaurants": [{
		"Id": 4729,
		"Name": "SimShop Getr\u00e4nke & Snacks",
		"Address": "Pumpwerkstrasse 41",
		"Postcode": "8105",
		"City": "Regensdorf",
		"CuisineTypes": [{
			"Id": 30,
			"Name": "drinks"
		}],
		"Url": "https:\/\/www.eat.ch\/restaurant\/simshop-getr-nke-snacks\/menu",
		"IsSponsored": false,
		"NewnessDate": "2000-01-01T00:12:11Z",
		"IsNew": false,
		"IsTemporarilyOffline": false,
		"ReasonWhyTemporarilyOffline": false,
		"UniqueName": "simshop-getr-nke-snacks",
		"IsHalal": false,
		"IsOpenNowForDelivery": true,
		"IsOpenNowForCollection": false,
		"RatingStars": 4.89,
		"DeliveryCost": 5.9,
		"IsCollection": false,
		"IsDelivery": true,
		"Deals": [],
		"Logo": [{
			"StandardResolutionURL": "https:\/\/static.eat.ch\/mod_logos\/simshop-getr-nke-snacks.png"
		}],
		"NumberOfRatings": 9,
		"IsOpenNow": true,
		"DeliveryOpeningTime": "2018-08-13T08:30:00Z",
		"DefaultDisplayRank": 1
	}, {
		"Id": 4720,
		"Name": "Seven's Pizza & Kebab Oerlikon",
		"Address": "Nansenstrasse 16",
		"Postcode": "8050",
		"City": "Z\u00fcrich",
		"CuisineTypes": [{
			"Id": 9,
			"Name": "turkish"
		}, {
			"Id": 24,
			"Name": "kebab"
		}, {
			"Id": 23,
			"Name": "pizza"
		}],
		"Url": "https:\/\/www.eat.ch\/restaurant\/seven-s-pizza-kebab-oerlikon\/menu",
		"IsSponsored": false,
		"NewnessDate": "2000-01-01T00:12:11Z",
		"IsNew": false,
		"IsTemporarilyOffline": false,
		"ReasonWhyTemporarilyOffline": false,
		"UniqueName": "seven-s-pizza-kebab-oerlikon",
		"IsHalal": false,
		"IsOpenNowForDelivery": true,
		"IsOpenNowForCollection": false,
		"RatingStars": 4.82,
		"DeliveryCost": 0,
		"IsCollection": false,
		"IsDelivery": true,
		"Deals": [],
		"Logo": [{
			"StandardResolutionURL": "https:\/\/static.eat.ch\/mod_logos\/seven-s.png"
		}],
		"NumberOfRatings": 1473,
		"IsOpenNow": true,
		"DeliveryOpeningTime": "2018-08-13T13:30:00Z",
		"DefaultDisplayRank": 2
	}, {
		"Id": 4525,
		"Name": "Pizzeria La Casa",
		"Address": "Zweierstrasse 124",
		"Postcode": "8003",
		"City": "Z\u00fcrich",
		"CuisineTypes": [{
			"Id": 23,
			"Name": "pizza"
		}, {
			"Id": 1,
			"Name": "italian"
		}],
		"Url": "https:\/\/www.eat.ch\/restaurant\/pizzeria-la-casa\/menu",
		"IsSponsored": false,
		"NewnessDate": "2000-01-01T00:12:11Z",
		"IsNew": false,
		"IsTemporarilyOffline": false,
		"ReasonWhyTemporarilyOffline": false,
		"UniqueName": "pizzeria-la-casa",
		"IsHalal": false,
		"IsOpenNowForDelivery": true,
		"IsOpenNowForCollection": false,
		"RatingStars": 4.74,
		"DeliveryCost": 0,
		"IsCollection": false,
		"IsDelivery": true,
		"Deals": [],
		"Logo": [{
			"StandardResolutionURL": "https:\/\/static.eat.ch\/mod_logos\/pizzeria-la-casa.png"
		}],
		"NumberOfRatings": 1814,
		"IsOpenNow": true,
		"DeliveryOpeningTime": "2018-08-13T10:30:00Z",
		"DefaultDisplayRank": 3
	}, {
		"Id": 4087,
		"Name": "Telepizza Oerlikon",
		"Address": "D\u00f6rflistrasse 117",
		"Postcode": "8050",
		"City": "Z\u00fcrich",
		"CuisineTypes": [{
			"Id": 23,
			"Name": "pizza"
		}, {
			"Id": 1,
			"Name": "italian"
		}, {
			"Id": 8,
			"Name": "american"
		}],
		"Url": "https:\/\/www.eat.ch\/restaurant\/telepizza-oerlikon\/menu",
		"IsSponsored": false,
		"NewnessDate": "2000-01-01T00:12:11Z",
		"IsNew": false,
		"IsTemporarilyOffline": false,
		"ReasonWhyTemporarilyOffline": false,
		"UniqueName": "telepizza-oerlikon",
		"IsHalal": false,
		"IsOpenNowForDelivery": true,
		"IsOpenNowForCollection": false,
		"RatingStars": 4.7,
		"DeliveryCost": 0,
		"IsCollection": false,
		"IsDelivery": true,
		"Deals": [],
		"Logo": [{
			"StandardResolutionURL": "https:\/\/static.eat.ch\/mod_logos\/telepizza.png"
		}],
		"NumberOfRatings": 396,
		"IsOpenNow": true,
		"DeliveryOpeningTime": "2018-08-13T11:00:00Z",
		"DefaultDisplayRank": 4
	}, {
		"Id": 1984,
		"Name": "Negishi Sushi Bar Badenerstrasse",
		"Address": "Badenerstrasse 97",
		"Postcode": "8004",
		"City": "Z\u00fcrich",
		"CuisineTypes": [{
			"Id": 22,
			"Name": "sushi"
		}, {
			"Id": 4,
			"Name": "japanese"
		}],
		"Url": "https:\/\/www.eat.ch\/restaurant\/negishi-sushi-bar-badenerstrasse\/menu",
		"IsSponsored": false,
		"NewnessDate": "2015-02-01T15:32:55Z",
		"IsNew": false,
		"IsTemporarilyOffline": false,
		"ReasonWhyTemporarilyOffline": false,
		"UniqueName": "negishi-sushi-bar-badenerstrasse",
		"IsHalal": false,
		"IsOpenNowForDelivery": true,
		"IsOpenNowForCollection": false,
		"RatingStars": 4.53,
		"DeliveryCost": 5,
		"IsCollection": false,
		"IsDelivery": true,
		"Deals": [],
		"Logo": [{
			"StandardResolutionURL": "https:\/\/static.eat.ch\/mod_logos\/negishi-sushi-bar-zuerich.png"
		}],
		"NumberOfRatings": 413,
		"IsOpenNow": true,
		"DeliveryOpeningTime": "2018-08-13T11:00:00Z",
		"DefaultDisplayRank": 5
	}, {
		"Id": 2002,
		"Name": "La Favorita",
		"Address": "Birmensdorferstrasse 169",
		"Postcode": "8003",
		"City": "Z\u00fcrich",
		"CuisineTypes": [{
			"Id": 23,
			"Name": "pizza"
		}, {
			"Id": 24,
			"Name": "kebab"
		}, {
			"Id": 1,
			"Name": "italian"
		}, {
			"Id": 9,
			"Name": "turkish"
		}, {
			"Id": 5,
			"Name": "swiss"
		}],
		"Url": "https:\/\/www.eat.ch\/restaurant\/la-favorita\/menu",
		"IsSponsored": false,
		"NewnessDate": "2015-03-01T10:32:27Z",
		"IsNew": false,
		"IsTemporarilyOffline": false,
		"ReasonWhyTemporarilyOffline": false,
		"UniqueName": "la-favorita",
		"IsHalal": false,
		"IsOpenNowForDelivery": true,
		"IsOpenNowForCollection": false,
		"RatingStars": 4.52,
		"DeliveryCost": 0,
		"IsCollection": false,
		"IsDelivery": true,
		"Deals": [],
		"Logo": [{
			"StandardResolutionURL": "https:\/\/static.eat.ch\/mod_logos\/peperoncino.png"
		}],
		"NumberOfRatings": 2167,
		"IsOpenNow": true,
		"DeliveryOpeningTime": "2018-08-13T10:30:00Z",
		"DefaultDisplayRank": 6
	}, {
		"Id": 2657,
		"Name": "Nooch Asian Kitchen Steinfels",
		"Address": "Heinrichstrasse 267",
		"Postcode": "8005",
		"City": "Z\u00fcrich",
		"CuisineTypes": [{
			"Id": 11,
			"Name": "asian"
		}, {
			"Id": 10,
			"Name": "thai"
		}, {
			"Id": 4,
			"Name": "japanese"
		}, {
			"Id": 22,
			"Name": "sushi"
		}],
		"Url": "https:\/\/www.eat.ch\/restaurant\/nooch-asian-kitchen-steinfels\/menu",
		"IsSponsored": false,
		"NewnessDate": "2016-03-11T17:41:21Z",
		"IsNew": false,
		"IsTemporarilyOffline": false,
		"ReasonWhyTemporarilyOffline": false,
		"UniqueName": "nooch-asian-kitchen-steinfels",
		"IsHalal": false,
		"IsOpenNowForDelivery": true,
		"IsOpenNowForCollection": false,
		"RatingStars": 4.51,
		"DeliveryCost": 5,
		"IsCollection": false,
		"IsDelivery": true,
		"Deals": [],
		"Logo": [{
			"StandardResolutionURL": "https:\/\/static.eat.ch\/mod_logos\/nooch-asian-kitchen-steinfels.png"
		}],
		"NumberOfRatings": 363,
		"IsOpenNow": true,
		"DeliveryOpeningTime": "2018-08-13T11:00:00Z",
		"DefaultDisplayRank": 7
	}, {
		"Id": 3867,
		"Name": "Mr. Grillhaus ",
		"Address": "Schaffhauserstrasse 105",
		"Postcode": "8152",
		"City": "Opfikon",
		"CuisineTypes": [{
			"Id": 26,
			"Name": "burger"
		}, {
			"Id": 23,
			"Name": "pizza"
		}, {
			"Id": 24,
			"Name": "kebab"
		}, {
			"Id": 9,
			"Name": "turkish"
		}, {
			"Id": 8,
			"Name": "american"
		}],
		"Url": "https:\/\/www.eat.ch\/restaurant\/mr-grillhaus-\/menu",
		"IsSponsored": false,
		"NewnessDate": "2000-01-01T00:12:11Z",
		"IsNew": false,
		"IsTemporarilyOffline": false,
		"ReasonWhyTemporarilyOffline": false,
		"UniqueName": "mr-grillhaus-",
		"IsHalal": false,
		"IsOpenNowForDelivery": true,
		"IsOpenNowForCollection": false,
		"RatingStars": 4.51,
		"DeliveryCost": 0,
		"IsCollection": false,
		"IsDelivery": true,
		"Deals": [],
		"Logo": [{
			"StandardResolutionURL": "https:\/\/static.eat.ch\/mod_logos\/mr-grillhaus-.png"
		}],
		"NumberOfRatings": 578,
		"IsOpenNow": true,
		"DeliveryOpeningTime": "2018-08-13T10:30:00Z",
		"DefaultDisplayRank": 8
	}, {
		"Id": 2412,
		"Name": "Mirch & Marsala - Pizza Wagner",
		"Address": "Gujerstrasse 2",
		"Postcode": "8050",
		"City": "Z\u00fcrich",
		"CuisineTypes": [{
			"Id": 1,
			"Name": "italian"
		}, {
			"Id": 3,
			"Name": "greek"
		}, {
			"Id": 5,
			"Name": "swiss"
		}, {
			"Id": 7,
			"Name": "indian"
		}],
		"Url": "https:\/\/www.eat.ch\/restaurant\/mirch-marsala---pizza-wagner\/menu",
		"IsSponsored": false,
		"NewnessDate": "2015-10-30T14:23:37Z",
		"IsNew": false,
		"IsTemporarilyOffline": false,
		"ReasonWhyTemporarilyOffline": false,
		"UniqueName": "mirch-marsala---pizza-wagner",
		"IsHalal": false,
		"IsOpenNowForDelivery": true,
		"IsOpenNowForCollection": false,
		"RatingStars": 4.44,
		"DeliveryCost": 4.9,
		"IsCollection": false,
		"IsDelivery": true,
		"Deals": [],
		"Logo": [{
			"StandardResolutionURL": "https:\/\/static.eat.ch\/mod_logos\/mirch-marsala---pizza-wagner.png"
		}],
		"NumberOfRatings": 1868,
		"IsOpenNow": true,
		"DeliveryOpeningTime": "2018-08-13T15:00:00Z",
		"DefaultDisplayRank": 9
	}, {
		"Id": 368,
		"Name": "Lemon Food and Drinks",
		"Address": "Regensbergstrasse 175",
		"Postcode": "8050",
		"City": "Z\u00fcrich",
		"CuisineTypes": [{
			"Id": 23,
			"Name": "pizza"
		}, {
			"Id": 24,
			"Name": "kebab"
		}, {
			"Id": 1,
			"Name": "italian"
		}, {
			"Id": 9,
			"Name": "turkish"
		}],
		"Url": "https:\/\/www.eat.ch\/restaurant\/lemon-food-and-drinks\/menu",
		"IsSponsored": false,
		"NewnessDate": "2011-05-25T14:18:21Z",
		"IsNew": false,
		"IsTemporarilyOffline": false,
		"ReasonWhyTemporarilyOffline": false,
		"UniqueName": "lemon-food-and-drinks",
		"IsHalal": false,
		"IsOpenNowForDelivery": true,
		"IsOpenNowForCollection": false,
		"RatingStars": 4.41,
		"DeliveryCost": 0,
		"IsCollection": false,
		"IsDelivery": true,
		"Deals": [],
		"Logo": [{
			"StandardResolutionURL": "https:\/\/static.eat.ch\/mod_logos\/lemon-food-and-drinks.png"
		}],
		"NumberOfRatings": 929,
		"IsOpenNow": true,
		"DeliveryOpeningTime": "2018-08-13T11:00:00Z",
		"DefaultDisplayRank": 10
	}
]
}
export default topRestaurant;
