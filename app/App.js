import React from 'react';
import { Root } from './router/config';

const App = () => (
  <Root />
);

export default App;
