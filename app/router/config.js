import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';

import styles from '../files/styles';

import Menu from './stack/Menu';
import Reviews from './stack/Reviews';
import Info from './stack/Info';
import AboutUs from './stack/AboutUs';

export const Root = createStackNavigator({
	Menu: {
		screen: Menu,
		navigationOptions: {
			title: null,
			header: null,
		},
	},
	Reviews: {
		screen: Reviews,
		navigationOptions: {
			title: null,
			header: null,
		},
	},
  Info: {
		screen: Info,
		navigationOptions: {
			title: null,
			header: null,
		},
	},
	AboutUs: {
		screen: AboutUs,
		navigationOptions: {
			title: null,
			header: null,
		},
	},
},
{
	initialRouteName: 'Menu',
	mode: 'modal',
	headerMode: 'screen',
});
