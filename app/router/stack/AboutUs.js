import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Platform,
  Dimensions,
  Picker
} from 'react-native';

import styles from '../../files/styles';
import { connect } from 'react-redux';
import ModalDropdown from 'react-native-modal-dropdown';

import {
  CardSection,
  Button,
  TextInputView,
  Footer,
  Comment,
  Overall,
  WhereToFindUs,
  DeliveryCollection,
  NiHau,
  Header,
  Navigation,
  OrderNowPopUp,
  WhatsNew,
  MostPopularDishes,
  OurStores,
  Offers
} from '../../files/components';

import { StackActions, NavigationActions } from 'react-navigation';

import data from '../../data/menuAndPayment'
import toprest from '../../data/topRestaurant'
import dataComment from "./sortedReviews";

import PopupDialog, {
  DialogTitle,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
  FadeAnimation,
} from 'react-native-popup-dialog';

const slideAnimation = new SlideAnimation({ slideFrom: 'bottom' });
const scaleAnimation = new ScaleAnimation();
const fadeAnimation = new FadeAnimation({ animationDuration: 150 });


class AboutUs extends Component {

	constructor(props) {
		super(props);

		this.state = {
      showSecondNav: false
		};
	}

	componentDidMount() {

	}

  orderNow = () => {
    this.OrderNowPopUp.showPopUp()
  }

  handleScroll = (event) => {
    console.log(event.nativeEvent.contentOffset.y);
    if (event.nativeEvent.contentOffset.y >= 433 && !this.state.showSecondNav) {
      this.setState({showSecondNav: true})
    } else if (event.nativeEvent.contentOffset.y < 433 && this.state.showSecondNav) {
      this.setState({showSecondNav: false})
    }
  }

	render() {
		return (
      <View style={{ backgroundColor: '#f5f5f5', display: 'flex', flexDirection: 'column', marginTop: 22, flex: 1 }}>
        {this.state.showSecondNav ?  <Navigation navigation={this.props.navigation} navStyle={{position: 'absolute', top:0, left: 0, zIndex: 1}} /> : null}
        <ScrollView style={{height: height, position: 'relative'}} onScroll={this.handleScroll} scrollEventThrottle={16}>
          <Header orderNow={this.orderNow}  navigation={this.props.navigation} />
          <View style={styles.overallWrapp}>
            <NiHau />
          </View>
          <View style={styles.overallWrapp}>
            <WhatsNew />
          </View>
          <View style={styles.overallWrapp}>
            <MostPopularDishes />
          </View>
          <View style={styles.overallWrapp}>
            <Offers />
          </View>
          <View style={styles.overallWrapp}>
            <OurStores />
          </View>
          <Footer />
        </ScrollView>
        <PopupDialog
          ref={(popupDialogAllergy) => { this.popupDialogAllergy = popupDialogAllergy; }}
          containerStyle={styles.containerPopup}
          dialogStyle={styles.dialogAllergyStyle}
          dialogAnimation={slideAnimation}
        >
          <View style={styles.popupStyle}>
            <TouchableOpacity style={styles.wrapPopupCloseIcon} onPress={() => this.popupDialogAllergy.dismiss()}>
              <Image source={require('../../files/images/popup__cross.png')} style={styles.popupCloseIcon}/>
            </TouchableOpacity>
            <Text style={styles.popupAllergyHeader}>Any allergy or dietary requirements?</Text>
            <Text style={styles.popupSI}>If you have an allergy that could harm your health, or have religious requirements (such as halal or kosher), we strongly advise you to contact the restaurant directly before you place your order.</Text>
          </View>
        </PopupDialog>
        <OrderNowPopUp onRef={ref => (this.OrderNowPopUp = ref)} />
      </View>
		);
	}
}

const width = Dimensions.get('window').width; //full width
const height = Dimensions.get('window').height; //full height

function mapStateToProps (state) {
	return {

	}
}

function mapDispatchToProps (dispatch) {
	return {

	}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AboutUs);
