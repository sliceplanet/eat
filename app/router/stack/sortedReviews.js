import data from "../../data/reviews.js";
 const sortedReviews = data.Reviews.sort((a, b) => {
  return new Date(b.RateDate) - new Date(a.RateDate);
});
export default sortedReviews;
