import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Platform,
  Dimensions,
  Picker
} from 'react-native';

import styles from '../../files/styles';
import { connect } from 'react-redux';
import ModalDropdown from 'react-native-modal-dropdown';

import {
  CardSection,
  Button,
  TextInputView,
  Footer,
  Comment,
  Overall,
  WhereToFindUs,
  DeliveryCollection,
  NiHau,
  Header,
  Navigation,
  OrderNowPopUp,
  WhatsNew,
  MostPopularDishes
} from '../../files/components';

import { StackActions, NavigationActions } from 'react-navigation';

import data from '../../data/menuAndPayment'
import toprest from '../../data/topRestaurant'
import dataComment from "./sortedReviews";

import { Switch } from 'react-native-switch';
import SwitchButton from 'switch-button-react-native';
import Swiper from 'react-native-swiper';

import PopupDialog, {
  DialogTitle,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
  FadeAnimation,
} from 'react-native-popup-dialog';

const slideAnimation = new SlideAnimation({ slideFrom: 'bottom' });
const scaleAnimation = new ScaleAnimation();
const fadeAnimation = new FadeAnimation({ animationDuration: 150 });

class Reviews extends Component {

	constructor(props) {
		super(props);

    this.state = {
      starCount: 3.5,
      comments: dataComment,
      toggleComment: true,
      sort: 'Newest First',
      showSecondNav: false
    };
    this.textInput = React.createRef();
    this.successCode = React.createRef();
	}

	componentDidMount() {

	}

  sortCommentsByDate = (type = "asc") => {
    if (type === "asc") {
      let array = [...this.state.comments];
      let comments = array.sort((a, b) => {
        return new Date(a.RateDate) - new Date(b.RateDate);
      });
      this.setState({ comments });
    } else {
      let array = [...this.state.comments];
      let comments = array.sort((a, b) => {
        return new Date(b.RateDate) - new Date(a.RateDate);
      });
      this.setState({ comments });
    }
  };

  sortCommentsByStars = (type = "asc") => {
    if (type === "asc") {
      let array = [...this.state.comments];
      let comments = array.sort((a, b) => {
        return a.RatingAverage - b.RatingAverage;
      });
      this.setState({ comments });
    } else {
      let array = [...this.state.comments];
      let comments = array.sort((a, b) => {
        return b.RatingAverage - a.RatingAverage;
      });
      this.setState({ comments });
    }
  };

  handleChange = (idx, value) => {
    this.setState({sort: value});
    if (value === "Oldest First") {
      this.sortCommentsByDate("asc");
    } else if (value === "Newest First") {
      this.sortCommentsByDate("desc");
    } else if (value === "Highest Rated") {
      this.sortCommentsByStars("desc");
    } else if (value === "Lowest Rated") {
      this.sortCommentsByStars("asc");
    }
  }


  handleScroll = (event) => {
    console.log(event.nativeEvent.contentOffset.y);
    if (event.nativeEvent.contentOffset.y >= 433 && !this.state.showSecondNav) {
      this.setState({showSecondNav: true})
    } else if (event.nativeEvent.contentOffset.y < 433 && this.state.showSecondNav) {
      this.setState({showSecondNav: false})
    }
  }

  orderNow = () => {
    this.OrderNowPopUp.showPopUp()
  }

	render() {
		return (
      <View style={{ backgroundColor: '#f5f5f5', display: 'flex', flexDirection: 'column', marginTop: 22, flex: 1 }}>
        {this.state.showSecondNav ?  <Navigation navigation={this.props.navigation} navStyle={{position: 'absolute', top:0, left: 0, zIndex: 1}} /> : null}
        <ScrollView style={{height: height, position: 'relative'}} onScroll={this.handleScroll} scrollEventThrottle={16}>
          <Header orderNow={this.orderNow}  navigation={this.props.navigation} />
          <View style={styles.overallWrapp}>
            <Overall />
          </View>
          <View style={styles.commentWrapper}>
            <View style={styles.wrappForPicker}>
              <ModalDropdown
                onSelect={(idx, value) => this.handleChange(idx, value)}
                defaultValue={this.state.sort}
                options={['Newest First', 'Oldest First', 'Highest Rated', 'Lowest Rated', ]}
                style={styles.dropdown_3}
                textStyle={styles.textDropdown}
                dropdownTextStyle={styles.dropdown_3_dropdownTextStyle}
                dropdownTextHighlightStyle={styles.dropdown_3_dropdownTextHighlightStyle}
              />
            </View>
            <View style={styles.wrappForSwitcher}>
                <SwitchButton
                  onValueChange={(val) => this.setState({ toggleComment: !this.state.toggleComment })}
                  text2 = 'Reviews (10)'
                  text1 = 'Ratings (20)'
                  switchWidth = {250}
                  switchHeight = {44}
                  switchdirection = 'rtl'
                  switchBorderRadius = {100}
                  switchSpeedChange = {500}
                  switchBorderColor = '#d4d4d4'
                  switchBackgroundColor = '#fff'
                  btnBorderColor = '#006ac1'
                  btnBackgroundColor = '#006ac1'
                  fontColor = '#535353'
                  activeFontColor = '#fff'
              />
            </View>
            <View style={styles.comWrapp}>
              {this.state.toggleComment ? this.state.comments.map((item, i) => {
                  if (item.CustomerComments ){
                  return (
                    <Comment
                      textComment={item.CustomerComments}
                      authorComment={item.CustomerName}
                      dateComment={item.RateDate}
                      mark={item.RatingAverage}
                      key={i}
                    />
                  );
                }
                }) : this.state.comments.map((item, i) => {
                  if (!item.CustomerComments) {
                    return (
                      <Comment
                        textComment={item.CustomerComments}
                        authorComment={item.CustomerName}
                        dateComment={item.RateDate}
                        mark={item.RatingAverage}
                        key={i}
                      />
                    );
                  }
                }) }
            </View>
          </View>
          <Footer />
        </ScrollView>
        <PopupDialog
          ref={(popupDialogAllergy) => { this.popupDialogAllergy = popupDialogAllergy; }}
          containerStyle={styles.containerPopup}
          dialogStyle={styles.dialogAllergyStyle}
          dialogAnimation={slideAnimation}
        >
          <View style={styles.popupStyle}>
            <TouchableOpacity style={styles.wrapPopupCloseIcon} onPress={() => this.popupDialogAllergy.dismiss()}>
              <Image source={require('../../files/images/popup__cross.png')} style={styles.popupCloseIcon}/>
            </TouchableOpacity>
            <Text style={styles.popupAllergyHeader}>Any allergy or dietary requirements?</Text>
            <Text style={styles.popupSI}>If you have an allergy that could harm your health, or have religious requirements (such as halal or kosher), we strongly advise you to contact the restaurant directly before you place your order.</Text>
          </View>
        </PopupDialog>
        <OrderNowPopUp onRef={ref => (this.OrderNowPopUp = ref)} />
      </View>
		);
	}
}

const width = Dimensions.get('window').width; //full width
const height = Dimensions.get('window').height; //full height

function mapStateToProps (state) {
	return {

	}
}

function mapDispatchToProps (dispatch) {
	return {

	}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Reviews);
