import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Platform,
  Dimensions,
  Picker
} from 'react-native';

import styles from '../../files/styles';
import { connect } from 'react-redux';
import ModalDropdown from 'react-native-modal-dropdown';

import {
  CardSection,
  Button,
  TextInputView,
  Footer,
  Comment,
  Overall,
  WhereToFindUs,
  DeliveryCollection,
  NiHau,
  Header,
  Navigation,
  OrderNowPopUp,
  WhatsNew,
  MostPopularDishes
} from '../../files/components';
import ProductBlock  from '../../files/components/ProductBlock';

import { StackActions, NavigationActions } from 'react-navigation';

import data from '../../data/menuAndPayment'
import toprest from '../../data/topRestaurant'
import dataComment from "./sortedReviews";

import { Switch } from 'react-native-switch';
import SwitchButton from 'switch-button-react-native';
import Swiper from 'react-native-swiper';

import PopupDialog, {
  DialogTitle,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
  FadeAnimation,
} from 'react-native-popup-dialog';

const slideAnimation = new SlideAnimation({ slideFrom: 'bottom' });
const scaleAnimation = new ScaleAnimation();
const fadeAnimation = new FadeAnimation({ animationDuration: 150 });

class Menu extends Component {

	constructor(props) {
		super(props);

		this.state = {
      showSecondNav: false
		};
    this.textInput = React.createRef();
    this.successCode = React.createRef();
	}

  showScaleAnimationDialog = () => {
    this.scaleAnimationDialog.show();
  }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating
    });
  }

	componentDidMount() {

	}

  // Open order now popup
  orderNow = () => {
    this.OrderNowPopUp.showPopUp()
  }

  handleScroll = (event) => {
    if (event.nativeEvent.contentOffset.y >= 433 && !this.state.showSecondNav) {
      this.setState({showSecondNav: true})
    } else if (event.nativeEvent.contentOffset.y < 433 && this.state.showSecondNav) {
      this.setState({showSecondNav: false})
    }
  }

	render() {

		return (
      <View style={{ backgroundColor: '#f5f5f5', display: 'flex', flexDirection: 'column', marginTop: 22, flex: 1 }}>
        {this.state.showSecondNav ?  <Navigation navigation={this.props.navigation} navStyle={{position: 'absolute', top:0, left: 0, zIndex: 1}} /> : null}
        <ScrollView style={{height: height, position: 'relative'}} onScroll={this.handleScroll} scrollEventThrottle={16}>
          <Header orderNow={this.orderNow}  navigation={this.props.navigation} />
          <CardSection style={styles.mainContent}>
            <ProductBlock />
          </CardSection>
          <CardSection style={styles.containerSlider}>
            <Swiper style={styles.sliderWrapper} height={240}
              onMomentumScrollEnd={(e, state, context) => console.log('index:', state.index)}
              dot={<View style={{backgroundColor: 'rgba(0,0,0,.2)', width: 5, height: 5, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3}} />}
              activeDot={<View style={{backgroundColor: '#000', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3}} />}
              showsPagination={false} loop>
                <View style={styles.slide}>
                  <Image resizeMode='stretch' style={styles.imageSlide} source={require('../../files/images/sushi.png')} />
                  <Text style={styles.textSlide}>Dish Name</Text>
                </View>
                <View style={styles.slide}>
                  <Image resizeMode='stretch' style={styles.imageSlide} source={require('../../files/images/sushi.png')} />
                  <Text style={styles.textSlide}>Dish Name</Text>
                </View>
                <View style={styles.slide}>
                  <Image resizeMode='stretch' style={styles.imageSlide} source={require('../../files/images/sushi.png')} />
                  <Text style={styles.textSlide}>Dish Name</Text>
                </View>
                <View style={styles.slide}>
                  <Image resizeMode='stretch' style={styles.imageSlide} source={require('../../files/images/sushi.png')} />
                  <Text style={styles.textSlide}>Dish Name</Text>
                </View>
            </Swiper>
          </CardSection>
          <Footer />
        </ScrollView>
        <OrderNowPopUp onRef={ref => (this.OrderNowPopUp = ref)} />
      </View>
		);
	}
}

const width = Dimensions.get('window').width; //full width
const height = Dimensions.get('window').height; //full height

function mapStateToProps (state) {
	return {

	}
}

function mapDispatchToProps (dispatch) {
	return {

	}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Menu);
